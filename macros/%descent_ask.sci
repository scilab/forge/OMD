function x = %descent_ask(this)
  if this.stage == 0 then // (stage=0: first iteration)
    x = this.x0
  else
    x = this.x_k - this.step_k * this.dy_k
  end
endfunction
