function this = nmomd()
  this = mlist(['nmomd', 'ItMX', 'x0', 'x_init', 'f_init', 'upper','lower', 'kelley_restart', 'kelley_alpha', ...
                'simplex_relsize', 'log', 'stop', 'data_next', 'init', 'f_hist', 'x_hist']);

  this.ItMX = 100;
  this.x0   = [];
  this.kelley_restart  = %F;
  this.kelley_alpha    = 1e-4;
  this.simplex_relsize = 0.1;
  this.log             = %F;
  this.stop            = %F;
  this.upper           = 1e6;
  this.lower           = - 1e6;
  this.data_next       = [];
  this.init            = %T;
  this.x_init          = [];
  this.f_init          = [];
  this.x_hist          = [];
  this.f_hist          = [];
endfunction
