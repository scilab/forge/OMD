//=============================================================================
//Construct an RBF network
//=============================================================================
function [w,acond] = rbf_net(basis, af, x, f)

//Check number of input arguments
lhs = argn(1)
rhs = argn(2)
if rhs~=4 then
  printf("rbf_net:\n")
  printf("Number of arguments should be 4\n")
  abort   
end

[ndim,ndata]=size(x);
[nrf,ncf]=size(f);

//Check for consistency of dimensions
if ncf~=ndata | nrf~=1 then
  printf("rbf_net:\n")
  printf("Size of x and f are not same.\n")
  printf("f must be a row vector.\n")
  printf("Number of columns of x = number of columns of f.\n")
  abort   
end

//Construct coefficient matrix
for i=1:ndata
  for j=1:ndata
    A(i,j) = RBF_FUN(basis, af, x(:,i), x(:,j))
  end
end

//If basis=2, GAUSSIAN with constant
//   basis=3, THIN_PLATE_SPLINE
//   basis=4, THIN_PLATE_SPLINE
if basis==2 then
  A = [A, ones(ndata,1)]
  A = [A; ones(1,ndata), 0]
  f = [f, 0]
elseif basis==3 then
  A = [A, ones(ndata,1), x']
  A = [A; ones(1,ndata), zeros(1,1+ndim)]
  A = [A; x, zeros(ndim,1+ndim)]
  f = [f, zeros(1,1+ndim)]
elseif basis==4 then
  A = [A, ones(ndata,1), x']
  Z = []
  for i=1:ndim
    x1= x(i,:)'
    for j=i:ndim
      x2= x(j,:)'
      x3= x1 .* x2
      Z = [Z, x3]
    end
  end
  A = [A, Z]
  n1= ndim*(ndim+1)/2
  A = [A; ones(1,ndata), zeros(1,1+ndim+n1)]
  A = [A; x, zeros(ndim,1+ndim+n1)]
  A = [A; Z', zeros(n1,1+ndim+n1)]
  f = [f, zeros(1,1+ndim+n1)]
end

//Solve the linear system Aw=f
//w = A\(f')
//w = lsq(A, f', 1.0e-16)
Asp       = sparse(A)
[hand,rk] = lufact(Asp)
w         = lusolve(hand,f')

//Find condition number of A
if lhs==2 then
  acond = cond(A)
end
endfunction

//=============================================================================
//Gaussian Radial Basis Function
//case = 1 is GAUSSIAN
//case = 2 is GAUSSIAN with constant
//case = 3 is THIN PLATE SPLINE with power 2
//case = 4 is THIN PLATE SPLINE with power 4
//=============================================================================
function [fun] = RBF_FUN(basis, af, c, x)
  if basis==41 then 
  basis=4 
  end
  dr  = x - c
  d   = norm(dr)
  select basis
  case 1 then
    tmp = d/af
    tmp = tmp*tmp
    fun = exp(-tmp)
  case 2 then
    tmp = d/af
    tmp = tmp*tmp
    fun = exp(-tmp)
  case 3 then
    if d==0.0 then
       fun = 0.0
    else
       fun = d*d*log(d)
    end
  case 4 then
    if d==0.0 then
       fun = 0.0
    else
       fun = d^4*log(d)
    end
  case 5 then
     fun = sqrt(d^2 + af^2)
  else
     printf("RBF_FUN: This RBF is not defined\n")
     abort
  end
endfunction

//=============================================================================
//Gradient of RBF
//=============================================================================
function [fd] = RBF_FUN_D(basis, af, c, x)
   ndim= length(c)
   dr  = x - c
   d   = norm(dr)
   select basis
   case 1 then
      tmp = d/af
      tmp = tmp*tmp
      fun = exp(-tmp)
      fd  = -2*dr*fun/af/af
   case 3 then
      if d==0.0 then
         fd = zeros(ndim,1)
      else
         fd = (1 + 2*log(d)) * (x - c)
      end
   case 4 then
      if d==0.0 then
         fd = zeros(ndim,1)
      else
         fd = d^2 * (1 + 4*log(d)) * (x - c)
      end
   else
      printf("RBF_FUN_D: This RBF is not defined\n")
      abort
   end
endfunction

//=============================================================================
//Hessian of RBF
//=============================================================================
function [fdd] = RBF_FUN_DD(basis, af, c, x)
   ndim= length(c)
   dr  = x - c
   d   = norm(dr)
   select basis
   case 1 then
      tmp = d/af
      tmp = tmp*tmp
      fun = exp(-tmp)
      fdd = (2/af/af)*(2*(dr/af)*(dr/af)' - eye(ndim,ndim))*fun
   case 3 then
      printf("RBF_FUN_DD: Second derivative not defined for this RBF\n")
      abort
   case 4 then
      if d==0.0 then
         fdd = zeros(ndim,ndim)
      else
         fdd = 2*(3+4*log(d))*dr*dr' + d^2*(1+4*log(d))*eye(ndim,ndim)
      end
   else
      printf("RBF_FUN_DD: This RBF is not defined\n")
      abort
   end
endfunction

//=============================================================================
//Evaluate an RBF network
//Grad and Hess computation works only for GAUSSIAN RBF
//=============================================================================
function [fr,frd,frdd] = rbf_eval(basis, af, x, f, w, xe)

//Check number of input arguments
[lhs,rhs] = argn()
if rhs~=6 then
   printf("rbf_eval: Number of arguments should be 6\n")
   abort
end

[ndim,ndata]=size(x);
[nrf,ncf]=size(xe);

//Check for consistency of dimensions
if nrf~=ndim then
   printf("rbf_eval:\n")
   printf("Size of x and xe are not consistent.\n")
   printf("Number of row of x = number of rows of xe.\n")
   abort
end

//Construct coefficient matrix
for i=1:ncf
   A = []
   for j=1:ndata
      AA = RBF_FUN(basis, af, x(:,j), xe(:,i))
      A  = [A, AA]
   end
   if basis==2 then
      A = [A, 1] 
   elseif basis==3 then
      A = [A, 1, xe(:,i)'] 
   elseif basis==4 then
      A = [A, 1, xe(:,i)'] 
      Z = []
      for k=1:ndim
         x1= xe(k,i)
         x2= xe(k:ndim,i)'
         x3= x1 * x2
         Z = [Z, x3]
      end
      A = [A, Z]
   end
   fr(i) = A*w
end

fr = fr'

//Hessian matrix from quadratic term
if lhs>=2 & basis==4 then
   count=(ndata+1+ndim)+1
   for id=1:ndim
      wm(id,id) = 2*w(count)
      count     = count + 1
      for jd=id+1:ndim
         wm(id,jd) = w(count)
         wm(jd,id) = w(count)
         count     = count + 1
      end
   end
end

//Compute gradient
if lhs>=2 then

   frd = []
   for i=1:ncf
      A = []
      for j=1:ndata
         AA = RBF_FUN_D(basis, af, x(:,j), xe(:,i))
         A  = [A, AA]
      end
      fd = A*w(1:ndata)

      if basis==3 then
         fd = fd + w(ndata+2:$)
      elseif basis==4 then
         fd = fd + w(ndata+2:ndata+2+ndim-1)
         fd = fd + wm*xe(:,i)
      end

      frd= [frd, fd]
   end

end

//Compute Hessian
if lhs==3 then

   frdd = []
   for i=1:ncf
      fdd = 0
      for j=1:ndata
         AA  = RBF_FUN_DD(basis, af, x(:,j), xe(:,i))
         AA  = w(j)*AA
         fdd = fdd + AA
      end

      if basis==4 then
         fdd = fdd + wm
      end

      frdd= [frdd, fdd]
   end

end

endfunction

//============================================================================
//Cost function of Rippa for GAUSSIAN RBF
//Basically, leave-one-out validation
//=============================================================================
function cost = rippa_cost(basis, x, f, af)

[ndim,ndata] = size(x)

for iaf=1:length(af)

//Construct coefficient matrix
for i=1:ndata
   for j=1:ndata
      A(i,j) = RBF_FUN(basis, af(iaf), x(:,i), x(:,j))
   end
end

//Sparse representation; actually A is full in this case
Asp       = sparse(A)
[hand,rk] = lufact(Asp)
w         = lusolve(hand, f')
acond     = cond(A)

if acond > 1/(%eps) then //If condition number is large
   Err = 1.0e20        //then dont compute error
else
   for i=1:ndata
      e      = zeros(ndata,1)
      e(i)   = 1
      wi     = lusolve(hand,e)
      Err(i) = w(i)/wi(i)
   end
end

//L1 norm 
cost(iaf) = sum( abs(Err) )/ndata
end
endfunction
