function y = apply_basis_func(x, bf)

  [n, d] = size(x)
  bf = matrix(bf, 1, -1)
  q = size(bf, 2)

  // replace 'x(i)' by 'x(,:i)' in bf
  for i = 1 : q
    for j = 1 : d
      bf(i) = strsubst(bf(i), 'x(' + string(j) + ')', 'x(:,' + string(j) + ')')
    end
  end
  
  // apply each basis function to X
  y = zeros(n, q)
  for i = 1 : q
    fun = 'fun' + string(i);
    deff('y = ' + fun + '(x)', 'y = ' + bf(i))
    y(:,i) = evstr(fun + '(x)')
  end

endfunction
