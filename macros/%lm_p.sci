function %lm_p(this)

  printf('*** Linear Model ***\n')
  
  printf('\n')
  printf('Coefficients:\n')
  disp_data(matrix(this.coefficients, 1, -1), '', ...
	    ['(Intercept)', this.basis_func])

  printf('\n')
  printf('Residuals:\n')
  
  v = [min(this.residuals), matrix(quart(this.residuals), 1, -1), ... 
       max(this.residuals)]
  disp_data(v, '', ['Min', '1Q', 'Median', '3Q', 'Max'])
  
endfunction
