function [yopt,xopt] = randexp_algo(simulator,xmin,xmax,ii_int,max_nb_anal)
// wrapped call to the randexp algorithm
// inputs :
//
// outputs :
// 
//*********************

if (isdef('xmin','local')) then
  if hasfield('xmin',simulator) then
    warning('both optimizer and simulator define xmin, keeping xmin from simulator');
    xmin = simulator.xmin;
  end
else
  if hasfield('xmin',simulator) then
    xmin = simulator.xmin;
  else
    error('no xmin defined');
  end
end

if (isdef('xmax','local')) then
  if hasfield('xmax',simulator) then
    warning('both optimizer and simulator define xmax, keeping xmax from simulator');
    xmax = simulator.xmax;
  end
else
  if hasfield('xmax',simulator) then
    xmax = simulator.xmax;
  else
    error('no xmax defined');
  end
end

if (~isdef('ii_int','local')) then
  ii_int = [];
end

if (~isdef('max_nb_anal','local')) then
  max_nb_anal = 20; // default value for max_nb_anal
end

optim_param = struct();
optim_param.max_nb_anal = max_nb_anal;
optim_param.xmin = xmin;
optim_param.xmax = xmax;
optim_param.ii_int = ii_int;

optimizer=randexp(optim_param);

// what follows is generic to a lot of optimizers
// and could be factorized out

// the ask and tell loop
while ~stop(optimizer);
  x = ask(optimizer);
  y = simulator(x);
  optimizer = tell(optimizer,x,y);
end

[yopt,xopt] = best(optimizer);

// print out results
disp(optimizer);

endfunction
