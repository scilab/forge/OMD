function this = %rsearch3_tell(this, x, y)
  this.nb_eval = this.nb_eval + 1
  select this.step
   case 0 then
    // next iteration (goto step 1)
    this.iter = this.iter + 1
    dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
    x_new = this.x_best + this.bias + dev
    while ~ and(x_new >= this.x_min & x_new <= this.x_max)
      this.bias = 0.5 * this.bias
      dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
      x_new = this.x_best + this.bias + dev
    end
    this.dev = dev
    this.step = 1
   case 1 then
    if y < this.y_best then
      this.x_best = x
      this.y_best = y
      this.bias = 0.2 * this.bias + 0.4 * this.dev
      // next iteration (goto step 1)
      this.iter = this.iter + 1
      dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
      x_new = this.x_best + this.bias + dev
      while ~ and(x_new >= this.x_min & x_new <= this.x_max)
	this.bias = 0.5 * this.bias
	dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
	x_new = this.x_best + this.bias + dev
      end
      this.dev = dev
    else
      // don't iterate (goto step 2)
      this.step = 2
    end
   case 2 then
    if y < this.y_best then
      this.x_best = x
      this.y_best = y
      this.bias = this.bias - 0.4 * this.dev
    else
      this.bias = 0.5 * this.bias
    end
    // next iteration (goto step 1)
    this.iter = this.iter + 1
    dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
    x_new = this.x_best + this.bias + dev
    while ~ and(x_new >= this.x_min & x_new <= this.x_max)
      this.bias = 0.5 * this.bias
      dev = grand(1, 'mn', zeros(this.d, 1), this.Cov)'
      x_new = this.x_best + this.bias + dev
    end
    this.dev = dev
    this.step = 1
  end
endfunction
