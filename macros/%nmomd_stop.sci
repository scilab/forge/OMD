function out = %nmomd_stop(this)
  this.stop = this.ItMX <= 0;
  out = this.stop;
endfunction
