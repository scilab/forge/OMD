// Here is a routine related to penalizing 
// objective functions, ordering them, etc.
//
// *** Often used symbols ***
// nd : number of designs to order
// mg : number of inequality constraints
// mf : number of objectives to minimize
// f_hist : matrix of objectives, nd*mf
// g_hist : matrix of inequality constraints, nd*mg
// pg : vector of mg constraints penalty parameters (>=0)
// pf : vector of mf objective penalty parameters (>=0)
//
//  Authors : Rodolphe Le Riche

//***************************
function [iorder,is_feas,fp]=ord_lin_pena(nb_ofs,nb_constraints,f_hist,g_hist,pg,pf)
// Order by increasing penalized objective function fp
// fp(x) = \sum_i=1^mf pf(i)f_i(x) + \sum_i=1^mg pg(i)*max(0,g_i(x))
// the first is the best for positive penalties 
// and min f_i s.t. g_i <= 0 problems.
//
// [iorder,is_feas,fp]=ord_lin_pena(f_hist,g_hist,pg)
//   assumes pf = ones*(1,mf) and
// [iorder,is_feas,fp]=order_lin_penal(f_hist,g_hist)
//    further assumes pg = ones*(1,mg)
//
//   iorder : fp(iorder) is the ordering of fp in increasing order, 
//     i.e., iorder(1) is the best solution (if min fp) etc.
//   is_feas : is_feas(i)==%t if i-th solution is feasible
//   fp : vector of penalized objective functions. fp(i) is a scalar.
//
// Developper's notes: eventually, we might want to pass the 
//   type of penalty function as an argument
//


 [nlhs,nrhs]=argn();
 if (nrhs < 6) then
    pf = ones(1,nb_ofs);
 end
 if (nrhs < 5) then
    pg = ones(1,nb_constraints);
 end
 
 [fp]= calc_lin_pen(nb_ofs,nb_constraints,f_hist,g_hist,pg,pf);
 [s,iorder]=gsort(fp,'r','i');
 if (nb_constraints>0) then
   is_feas = (max(g_hist,'c')<=0);
 else
   nd = size(f_hist,'r');
   is_feas = (ones(nd,1)>zeros(nd,1));
 end
endfunction
