function x = %rsearch3_ask(this)
  select this.step
   case 0 then
    x = this.x0
   case 1 then
    x = this.x_best + this.bias + this.dev
   case 2 then
    x = this.x_best + this.bias - this.dev
    // project x on the domain boudary if not inside
    i = x < this.x_min
    x(i) = this.x_min(i)
    i = x > this.x_max
    x(i) = this.x_max(i)  
  end
endfunction
