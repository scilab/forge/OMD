function i = pareto_front(x)
  n = size(x, 'r')
  j = (zeros(1, n) ~= 0)
  for i = 1 : n
    x_i = ones(n - 1, 1) .*. x(i,:)
    x_ci = x((1 : n) ~= i,:)
    j(i) = and(or(x_i < x_ci, 'c'))
  end
  i = find(j)
endfunction
