function x = %nmomd_ask(this)
  if this.init then
    // We set the initial simplex
    for i=1:length(this.x0)+1
      this.x_init(:,i) = this.x0 + this.simplex_relsize*0.5* ((this.upper - this.lower) .* rand(size(this.x0,1),size(this.x0,2)) + this.lower);
    end
  end
  x = this.x_init;
endfunction
