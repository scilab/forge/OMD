function this = %descent_tell(this, x, y)
  if this.stage == 0 then // (stage=0: first iteration)
    this.x_k = x
    this.y_k = y(1)
    this.dy_k = y(2)
    this.iter = this.iter - 1
    this.step_k = this.step
    this.stage = 1
  else
    if y(1) > this.y_k then
      this.step_k = this.step_k /2
    else
      this.x_k = x
      this.y_k = y(1)
      this.dy_k = y(2)
      this.iter = this.iter - 1
      this.step_k = this.step
    end
  end
  this.eval = this.eval + 1
endfunction
