function x = %rsearch2_ask(this)
  if this.nb_eval == 0 then
    x = this.x0
  else
    x = this.x_best + this.dev
  end
endfunction
