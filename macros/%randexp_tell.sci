function out = %randexp_tell(this,x,opt_criteria)
  // make x, f, and g as rows for the history keeping
  // one row per design.
  this.nb_anal=this.nb_anal+1;
  x=matrix(x,1,-1); // x as a row
  has_of = hasfield("objective",opt_criteria);
  has_constraints = hasfield("constraint",opt_criteria);
  if has_of then 
    f=matrix(opt_criteria.objective,1,-1);
  end
  if has_constraints then
    g=matrix(opt_criteria.constraint,1,-1);
  end
  if (size(this.x_hist,'r')==0) then // first call
    // assign memory once for all
    n = length(this.xmin);
    this.x_hist=zeros(this.max_nb_anal,n);
    if has_constraints then
      nb_constraints = length(g);
      this.g_hist=zeros(this.max_nb_anal,nb_constraints);
    end
    if has_of then
      nb_ofs = length(f);
      this.of_hist=zeros(this.max_nb_anal,nb_ofs);
    end
  end
  this.x_hist(this.nb_anal,:)=x;
  if has_of then
    this.of_hist(this.nb_anal,:)=f;
  end
  if has_constraints then
    this.g_hist(this.nb_anal,:)=g;
  end
  // keep the user informed
  printf(' randexp analysis no %i done \r',this.nb_anal);
  out = this;
endfunction
