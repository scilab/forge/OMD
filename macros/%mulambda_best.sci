function [yopt, xopt] = %mulambda_best(this)
  [m, k] = min(this.parents_y)
  yopt = m
  xopt = this.parents_x(k, :)
endfunction
