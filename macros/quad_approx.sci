function [a0,G,H] = quad_approx(X,Y)
    //***********************
    // least square approximation of (X,Y) by 
    // the quadratic polynomial 
    // a0 + G*x + 0.5*x'*H*x
    //  where x is a real vector with ndim rows
    //
    //
    // inputs  
    //    X : ndim * m matrix where m is the number 
    //        of data points.
    //    Y : m*1 vector of function to approximate 
    //        values at X.
    // outputs
    //    the coefficients of the quadratic polynomial
    //    a0 : 1*1 scalar
    //    G : 1*ndim
    //    H : ndim*ndim, symmetric
    //    for a total of (ndim^2+3*ndim+2)/2 coefficients
    //
    //  Note : the least square problem is solved 
    //  using a pseudo-inverse matrix. It should therefore 
    //  still work when m, the number of data points is less
    //  than the number of coefficients, (ndim^2+3*ndim+2)/2.
    //  In this case, you should get the "flatest" approximating 
    //  polynomial.
    //
    //  Rodolphe Le Riche and Alexandre Franquet, 2009.
    //***********************


    // making the C matrix of quadratic approximation coefficients
    // Note : works for ndim dimensions ;-)

    [ndim,m] = size(X);
    l = (ndim*ndim + 3*ndim+2)/2; // number of coefficients
    C = zeros(m,l);

    for i=1:m, // loop on number of data points
      C(i,1)=1;
      for j=1:ndim,
        C(i,j+1) = X(j,i);
      end
      for j=1:ndim,
        C(i,ndim+1+j) = 0.5*X(j,i)*X(j,i);
      end
      kk = 1;
      for j=1:ndim,
        for k=(j+1):ndim,
          C(i,2*ndim+1+kk) = X(j,i)*X(k,i);
          kk = kk+1;
        end
      end
    end // end loop on number of data points

    // find coefficients of quadratic approximation by 
    // solving the normal equations of linear least squares
    // pseudo-inverse version
    Cinv = pinv(C);
    A = Cinv*Y; // A is l by 1

    // reconstruct the matrices of constant, linear and 
    // quadratic contributions
    G = zeros(1,ndim);
    H = zeros(ndim,ndim);
    a0 = A(1);
    for j=1:ndim,
      G(j) = A(1+j);
    end
    for j=1:ndim,
      H(j,j) = A(1+ndim+j);
    end
    k = 1;
    for i=1:ndim,
      for j=(i+1):ndim,
        H(i,j) = A(1+2*ndim+k);
        H(j,i) = A(1+2*ndim+k);
        k = k+1;
      end
    end

endfunction
