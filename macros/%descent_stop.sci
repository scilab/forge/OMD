function out = %descent_stop(this)
  if this.stage == 0 then
    out = (this.iter <= 0)
  else
    out = (this.iter <= 0) | (this.step_k * sqrt(sum(this.dy_k .^2)) <= this.eps)
  end
endfunction
