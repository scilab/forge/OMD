function [y, v] = %lm_e(X, this)

  [n, d] = size(X)
  
  F = [ones(n, 1), apply_basis_func(X, this.basis_func)]
  
  y = F * this.coefficients
  v = sum(this.residuals).^2 / (n - d - 1)

endfunction
