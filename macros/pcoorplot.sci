function [sel, perm, bg] = pcoorplot(x, labels, style, clickable, sel, perm, bg)

  // argument checking
  rhs = argn(2)
  if rhs < 1 then, error(39), end
  [nr, nc] = size(x)
  m1 = min(x, 'r')
  m2 = max(x, 'r')
  if rhs < 2 then, labels = 'X' + string(1 : nc), end
  labels = matrix(labels, -1)
  if rhs < 3 then, style = ones(1, nr), end
  if rhs < 4 then, clickable = %t, end
  if rhs < 5 then, sel = [m1; m2], end
  if rhs < 6 then, perm = 1 : nc, end
  perm = matrix(perm, 1, -1) // make as row
  if rhs < 7 then, bg = %f, end
  
  // standardization of the matrices x and sel on [0,1]
  x = (x - (m1 .*. ones(nr, 1))) ./ ((m2 - m1) .*. ones(nr, 1))
  sel = (sel - (m1 .*. ones(2, 1))) ./ ((m2 - m1) .*. ones(2, 1))
  
  // saving initial parameters (for reinit button)
  sel_init = sel
  perm_init = perm
  
  // initialize graphic window
  e = gcf();
  gwin = e.figure_id;
  addmenu(gwin, 'Reinitialize');
  addmenu(gwin, 'Background');
  addmenu(gwin, 'Help');
  addmenu(gwin, 'Exit');
  
  // main loop
  exit_loop = %f;
  while ~exit_loop
    // j: indices of selected rows
    j = (zeros(nr, 1) == 0)
    for i = 1 : nc
      j = j & (x(:,i) >= sel(1,i)) & (x(:,i) <= sel(2,i))
    end
    nj = length(find(j))

    // initialize plot
    clf();
    plot2d(0, 0, -1, '012', rect = [1, 0, nc, 1]);
    xpolys([1 : nc; 1 : nc], [zeros(1, nc); ones(1, nc)]);
    a              = gca();
    a.x_ticks      = tlist(['ticks', 'locations', 'labels'], (1 : nc)', labels(perm));
    a.sub_ticks(1) = 0;
    a.axes_visible = ['on', 'off', 'off'];
    
    // plot background (unselected lines)
    if bg then
      if nj < nr then
	xpolys((1:nc)' .*. ones(1, nr - nj), x(~j,perm)', color('gray') * ones(1, nr - nj));
      end
    end
    
    // plot (selected) lines
    if nj > 0 then
      xpolys((1:nc)' .*. ones(1, nj), x(j,perm)', style(j));
    end
    
    // plot selection marks
    plot2d(1:nc, sel(1,perm), style = -9);
    plot2d(1:nc, sel(2,perm), style = -9);

    // mouse click handling
    if clickable then
      [c_i, c_x, c_y, c_w, c_m] = xclick();
      if c_i == -100 then // (close graphic window)
	exit_loop = %t;
      elseif c_m == 'execstr(Reinitialize_' + string(gwin) + '(1))' then
	sel = sel_init;
	perm = perm_init;
      elseif c_m == 'execstr(Background_' + string(gwin) + '(1))' then
	bg = ~bg;
      elseif c_m == 'execstr(Exit_' + string(gwin) + '(1))' then
	exit_loop = %t;
      elseif c_m == 'execstr(Help_' + string(gwin) + '(1))' then
	help('pcoorplot');
      else
	c_x = max(c_x, 1);
	c_x = min(c_x, nc);	
	i = round(c_x);
	if or(c_i == [0, 3]) then // (left button pressed or clicked)
	  if abs(c_x - i) < .3 then
	    // select upper bound
	    sel(2, perm(i)) = min(max(c_y, sel(1, perm(i))), 1);
	  else
	    // permutation
	    i = floor(c_x);
	    tmp = perm(i);
	    perm(i) = perm(i + 1);
	    perm(i + 1) = tmp;
	  end
	elseif or(c_i == [2, 5]) then // (right button pressed or clicked)
	  if abs(c_x - i) < .3 then
	    // select lower bound
	    sel(1, perm(i)) = max(min(c_y, sel(2, perm(i))), 0);
	  end
	end
      end
    else
      exit_loop = %t;
    end
  
  end

  // delete custom menus
  delmenu(gwin, 'Reinitialize')
  delmenu(gwin, 'Background')
  delmenu(gwin, 'Help')
  delmenu(gwin, 'Exit')
  
  // de-standardization of sel
  sel = sel .* ((m2 - m1) .*. ones(2, 1)) + (m1 .*. ones(2, 1));
endfunction
