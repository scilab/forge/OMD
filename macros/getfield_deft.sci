function val = getfield_deft(f, l, v)
  
  allf = getfield(1, l)
  if typeof(l) == 'st' then
    allf = allf(3:$)
  else
    allf = allf(2:$)
  end
  // (cf help('mtlb_isfield')... )
  
  if or(allf == f) then
    val = getfield(f, l)
  else
    val = v
  end

endfunction