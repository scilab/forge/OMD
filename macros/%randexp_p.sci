function %randexp_p(this)  
  // saves results and displays a run summary
  max_nb_prints = 20;
  nb_ofs = size(this.of_hist,'c');
  nb_constraints = size(this.g_hist,'c');
  [iorder,is_feas,fp]=ord_lin_pena(nb_ofs,nb_constraints,this.of_hist,this.g_hist);
  save_file_name = 'randexp.res';
  save(save_file_name,this,iorder,is_feas,fp);
  printf("\n*** randexp ***\n");
  printf(" %i simulations done, results saved in ",this.max_nb_anal);
  printf(save_file_name);
  printf("\n");
  nb_prints = min(length(iorder),max_nb_prints);
  printf(" The %i best points are :\n",nb_prints);
  printf(' Point_id   is_feasible  penalized_of \n');
  for i=1:nb_prints,
    printf('    %i   %i   %e \n',iorder(i),bool2s(is_feas(iorder(i))),fp(iorder(i)));
  end 
endfunction
