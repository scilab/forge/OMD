function [x,optdata,outrec]=blvm(x0,func,gradfunc,xmin,xmax,param)
  
//% Levenberg-Marquardt algo for weighted least squares
//% uses hasfield from OMD-toolbox
//% [res]=func(x) returns the residues vector w.r.t. x (user defined)
//% [res,dres]=gradfunc(func,x,delta) returns the residues vector and its gradient w.r.t. x (user defined)
//%
//% This algorithm implements a Levenberg-Marquardt method with bounds handling. Please report to the following
//% workbook from F.Guyon and R.Leriche about the LVM principle and the bounds handling : 
//% http://www.emse.fr/~leriche/publications.html
//%
//%
//%
//%Input parameters:
//%
//% -x0 (real vector): a vector line/column, initial vector
//% -func (function): a function defined as [residu]=cost_function(x0)
//% -gradfunc (function): a gradient function defined as [residu,grad(residu)]=grad(cost_fct,x0,epsdiff) (with epsdiff=(finite volumes step))
//% -xmin (real vector): lower bound on x0
//% -xmax (real vector): uper bound on x0
//% -param (struct): an optional parameter structured list 
//%   *param.max_iter (integer) = maximum LVM useful steps (default is 500)
//%   *param.stop_crit (real) = tolerance on cost function (default is 1e-8)
//%   *param.lambda (real) = first lagrangian coefficient. The regularization factor. (default is 1)
//%   *param.nu (real>=1)= dividing ratio for lamda at each iteration. The speed at which lambda changes. (default is 2)
//%   *param.deltadiff (real) = epsdiff (finite volumes step) for the grad function (default is 1e-8)
//%   *param.xtol (real) = tolerance on x, stopping criterion on relative change in x (default is 1e-25)
//%   *param.lambdamin (real) = lower limit for lambda (default is 1e-18)
//%   *param.printout (boolean) = printout options (default is True)
//%   *param.w (sparse matrix) = weight matrix (default is empty) TODO : never used in this blvm version. "w" option might be deleted.
//%   *param.vsmall (real) = used in boundary violations (default is 1e-10)
//%   *param.timeLimit (real) = algorithm stop at maximum timeLimit (default is 1e20)
//%
//%Ouput parameters:
//%
//% -x (real vector) : solution vector
//% -optdata (struct) : structured list with infos on the optimised solution
//%    *optdata.res (real matrix) = residu at blvm final vector
//%    *optdata.dres (real matrix) = grad(residu) at blvm final point
//%    *optdata.iteration (integer) = number of optimisation iterations
//%    *optdata.lambda (real) = lambda at blvm final vector
//%    *optdata.time (real) = final time in seconds
//%    *optdata.lmmax (vector) = lagrangian multipliers "mu" (related to max bounds activation) at optimum
//%    *optdata.lmmin (vector) = lagrangian multipliers "gamma" (related to min bounds activation) at optimum
//%    *optdata.ID (integer) = "Algorithm blvm v1.0" version of blvm
//% -outrec (struct) : structured list with recorded infos
//%    *outrec.gradfunc_calls (integer) = number of grad function calls
//%    *outrec.func_calls (integer) = number of function calls
//%    *outrec.xrec (real matrix) = recorded x vectors
//%    *outrec.lambdarec (real vector) = recorded lambdas
//%    *outrec.PIrec (real vector) = recorded Performance Info ((residu'*residu)/(2*nexp))
//%    *outrec.badcond_nb (integer) = number of bad conditioned matrix encountered during boundary constraints regulation

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%		Version 1.0
//%
//%		Rodolphe Le Riche (leriche@emse.fr) and Alexandre Franquet (franquet@emse.fr), 2009.
//%		Ecole Nationale Supérieure des Mines de Saint-Etienne
//%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    tic();
    stacksize(30e6);//%A huge stacksize could be useful here...

    outrec=struct();
    optdata=struct();

    //%======================================
    //% Default values and parameter settings
    //%======================================

    max_iter=500;
    stop_crit=1.e-8;
    lambda=1.;
    nu=2.;
    deltadiff=1.e-8;
    xtol=1.e-25;
    lambdamin=1.e-18;//% min of lambda. Increase for ill-posed problems 
                   //% where linear system gets ill-conditionned, but 
                   //% progress might then slow down.
    printout=%t;
    w=%f;
    vsmall = 1.e-10; //% used in checking boundary violations
    timeLimit = 1e20;
    
    listofparam=['max_iter','stop_crit','lambda','nu','deltadiff','xtol','lambdamin','printout','w','vsmall','timeLimit'];

    if exists('param') then
        for i=1:length(length(listofparam))
            if hasfield(listofparam(i),param) then
                execstr(listofparam(i)+'= param.'+listofparam(i));
            end;
        end;
    end;

        
    //%%%%%%%%%%%%%%Default values definition done%%%%%%%%%%%%%%%%%%%%%%%%%%

    //%==========================================
    //%====Data initialisation and first loop====
    //%==========================================

    x = matrix(x0,-1,1);//% Vector in column
    x_new=x;
    [res,dres] = gradfunc(func,x,deltadiff);
    n = length(x);  //% number of variables
    nexp  = size(res,1);  // number of experimental points
    xstep=x;

    if w==%f then
        w = speye(nexp,nexp);
    else
        w=sparse(w);
    end;

    badcond_nb=0;
    sW=w'*w;//%Squared weight matrix
    mdPI = - dres'*sW*res;//%2nd member
    Hlin = dres'*sW*dres;//%Linear part of hessian
    need_grad = 0;//Need gradiant calculation at next step
    iteration = 1;//% Number of useful (improved) steps
    gradfunc_calls=1//grad has been called 1 time
    func_calls=0;//We will add calls of func in grad later

    SSE = res'*res; //% Sum of squared errors
    PI=SSE/2;//% Performance index

    //% Recording data : we build arrays in order to limit the resizing of these matrix
    PIrec=ones(50,1);
    xrec=ones(50,n);
    lambdarec=ones(50,1);
    PIrec(1) = PI; // A vector containing the accumulated SSE
    xrec(1,1:n) = x';
    lambdarec(1) = lambda;
    lagmult = zeros(n,1);

    //%%%%%%%%%%%%%%Data initialisation done%%%%%%%%%%%%%%%%%%%%%%%

    //%================================
    //%====Levenberg Marquardt loop====
    //%================================

    while iteration<max_iter & PI >= stop_crit & lambda<=1e25 & norm(xstep) >= xtol*norm(x) & toc()<=timeLimit
        if need_grad==1 then //% Need to re-calculate gradient ?
            [res,dres] = gradfunc(func,x,deltadiff);
            gradfunc_calls=gradfunc_calls+1;
            mdPI = - dres'*sW*res;
            Hlin = dres'*sW*dres;
            need_grad = 0;
        end;
    
        //% lvm hessian
        H = Hlin + lambda*eye(n,n);
      
        //% lvm step
        xstep = linsolve(H,-mdPI);
    
        //% -- Compute a priori iterate --
        x_new = x + xstep;  
        
        
        //%=================================
        //%==Boundary conditions checking===
        //%=================================
      
        badcond = %F;
        ifixed = []; //% variables fixed to the bound 
        actbnd = zeros(n,1); //% xx - xbound for active constraints
        chkbnd = zeros(n,1);
        nviol = 0;
        for i = 1:n
            if x_new(i)<xmin(i) then
                chkbnd(i) =  x_new(i)-xmin(i) ;
                nviol = nviol +1;
            elseif x_new(i)>xmax(i) then
                chkbnd(i) =  x_new(i)-xmax(i) ;
                nviol = nviol +1;
            end;
        end;
        
        lagmult = zeros(n,1);
        
        if nviol>0 then
            kkt = 0;
        else 
            kkt = 1;
        end;        
        
        while kkt==0 
            //% Add the most critical constraint
            if nviol>0 then
                [vcrit,icrit] = max(abs(chkbnd));   //% most critical constraint
                if printout==%t then
                    disp(' new active bound on variable ');
                    disp(icrit);
                end;
                ifixed = [ifixed;icrit]; 
                actbnd(icrit) = chkbnd(icrit);
            end;

            //% remove constraint with smallest lm <0, if no constraint
            //% has been added.

            icorm = find((lagmult(ifixed))<0);
            if length(icorm) > 0 & nviol==0 then
                //% using random nb instead of a tedious enumeration ... might 
                //% be improved if lots of nasty constraints
                shf = int(length(icorm)*rand())+1;       
                ilmmin = icorm(shf);
                if printout==%t then
                    disp('Bound on the variable no longer active ');
                    disp(ifixed(ilmmin));
                end;
                actbnd(ifixed(ilmmin)) = 0;
                ifixed(ilmmin) = [];
            end;

            HB = H;
            lhs = mdPI;
            for j=1:length(ifixed)
                i = ifixed(j);
                if sign(actbnd(i))>0 then
                    xstep(i) = xmax(i)-x(i);
                else 
                    xstep(i) = xmin(i)-x(i);
                end;
                HB(:,i) = 0; 
                HB(i,i) = sign(actbnd(i));
                lhs = lhs - xstep(i)*H(:,i);
            end;

            //% HE founds a matlab case where this system might get ill-conditionned while the 
            //% original system is ok. As a fix, increase lambda.
            if cond(HB)>1.e20 then
                if printout==%t then
                    disp('Bad conditionning of the system while fixing bounds');
                    disp('Trying to get through by increasing lambda');
                end;
                H = H +(nu-1)*lambda*eye(n,n);
                lambda= lambda*nu;
                badcond = %T;
                badcond_nb=badcond_nb+1;
            end;
    
            xstep_new = linsolve(HB,-lhs);
            lagmult = zeros(n,1);
            lagmult(ifixed) = xstep_new(ifixed);
            xstep_new(ifixed) = xstep(ifixed); 
            xstep = xstep_new;

            //% Are we done ?
            chkbnd = zeros(n,1);
            nviol = 0;
            x_new = x + xstep
            
            for i = 1:n
                //% there are numerical errors when enforcing bound limits, so enforce 
                //% only weak respect (use vsmall).
                if x_new(i)+vsmall <xmin(i) then 
                    chkbnd(i) =  x_new(i)-xmin(i);
                    nviol = nviol+1;
                elseif x_new(i) - vsmall >xmax(i) then
                    chkbnd(i) =  x_new(i)-xmax(i);
                    nviol = nviol+1;
                end;
            end;

            if min(lagmult)>=-1.e-10 & nviol==0 then // consider that -1.e-10
                // is zero numerical
                kkt = 1;
                if printout==%t then
                    disp('Bounding constraints fixed');
                    disp('Variables at bounds');
                    disp(ifixed);
                    disp('Lagrangian multipliers ');
                    disp(lagmult);
                end;
            end;

        end; //% end while ~kkt = end of bound checking
        
        //%------The kkt conditions should be NOW satisfied---------------------------------------------   
    
        [res_new] = func(x_new);
        func_calls=func_calls+1;
        SSE_new = res_new'*res_new;
        PI_new=SSE_new/2;
   
        //% Update x and lambda
        if PI_new < PI then //% Update x if improvement

            //%Is our improvement step big enough ?
            
            linear_pred = (xstep'*(-mdPI)-lambda*xstep'*xstep)/(2*nexp);//%linear_pred is negative
            real_step = PI_new-PI;
               
            iteration = iteration + 1;
            x = x_new;
            PI = PI_new;
            need_grad = 1;
                              
            // Record data (matrix could be potentially resized by a 30-step
            if iteration>length(PIrec) then
                PIrec=[PIrec; ones(30,1)];
            end;
            if iteration>size(xrec,1) then
                xrec=[xrec; ones(30,n)];
            end;
            if iteration>length(lambdarec) then
                lambdarec=[lambdarec; ones(30,1)];
            end;
        
            PIrec(iteration) = PI_new; 
            xrec(iteration,:) = x_new';
            lambdarec(iteration) = lambda;
            
            if printout==%t then
                printf('iteration # %i   PI = %4.3e  lambda = %4.3e\r',iteration-1,PI,lambda); 
            end;
          
          
            if lambda>lambdamin & ~badcond & real_step<0.75*linear_pred then
                lambda = lambda/nu;
            elseif real_step>0.25*linear_pred & lambda*nu<=1e25 then
                lambda = lambda*nu;
            end;
          
        else //% Wider lambda           
            lambda=lambda*nu;
        end;   
    
    end; 
    //%%%%%%%%%%%%%%LVM loop done%%%%%%%%%%%%%%%%%%%%%%

    //%======================================================
    //%====Check stopping criterion and write output data====
    //%======================================================
    
    if (PI < stop_crit) then
        printf('PI=%e < stop_crit=%e, stop',PI,stop_crit);        
        outrec.flag=1;
    elseif lambda>1e25 then
        printf('Lambda=%e is too high>1e25, stop',lambda);
        outrec.flag=2;
    elseif norm(xstep) < xtol*norm(x) then
        printf('norm(xstep)=%e < xtol*norm(x)=%e, stop',norm(xstep),xtol*norm(x));
        outrec.flag=3;
    elseif iteration==max_iter then
        printf('max_iter=%i reached, stop',max_iter);
        outrec.flag=4;
    elseif toc()>timeLimit then
        printf('The time limit=%f has been reached, stop',timeLimit);
        outrec.flag=5;
    else
        disp('Unknown ending reason, stop');
        outrec.flag=6;
    end;

    if abs(x- x_new)>1e-10 then
        [res,dres] = gradfunc(func,x,deltadiff);
        gradfunc_calls=gradfunc_calls+1;
    end;

    outrec.gradfunc_calls=gradfunc_calls;
    outrec.func_calls=func_calls+gradfunc_calls*(n+1);//if grad by finite differences
    outrec.xrec=xrec(1:iteration,:);
    outrec.lambdarec=lambdarec(1:iteration);
    outrec.PIrec=PIrec(1:iteration);
    outrec.badcond_nb=badcond_nb;
    
    optdata.res=res;
    optdata.dres=dres;
    optdata.iteration=iteration;
    optdata.lambda=lambda; 
    optdata.time=toc();
    if sum(lagmult)~=0 then
        optdata.lmmax=sqrt(sign(actbnd)+1)./sqrt(2).*lagmult;
        optdata.lmmin=sqrt(1-sign(actbnd))./sqrt(2).*lagmult;
    else
        optdata.lmmax=" No lagrangian multipliers ";
        optdata.lmmin=" No lagrangian multipliers ";
    end;
    optdata.ID='Algorithm blvm';
    optdata.deltadiff=deltadiff;
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
endfunction


