function this = rsearch(param)
  algorithm = getfield_deft('algorithm', param, 'blind')
  select algorithm
   case 'blind' then
    this = rsearch1(param)
   case 'localized' then
    this = rsearch2(param)
   case 'enhanced' then
    this = rsearch3(param)
  else
    error('Unknown value for field algorithm')
  end
endfunction
