// adds a number to a filename
// ex : number_file('toto.txt', 10, 5) -> 'toto_00010.txt'

function fname_i = number_file(fname, i, padlen)
  if ~ exists('padlen') then
    padlen = 1
  end
  [path, basename, extension] = fileparts(fname)
  fd = '%0' + string(padlen) + 'i'
  fname_i = path + basename + '_' + msprintf(fd, i) + extension
endfunction
