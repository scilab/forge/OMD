function %keylist_p(this)
  field_names = getfield(1, this)
  field_names = field_names(2:$)
  n = size(field_names, 'c')
  v = emptystr(n, 1)
  for i = 1 : n
    //v(i) = string(getfield(i + 1, this))
    v(i) = sci2exp(getfield(i + 1, this))
  end
  table = [matrix(field_names, -1), v]
  table = justify(table, 'l')
  printf('%s: %s\n', table)
endfunction
