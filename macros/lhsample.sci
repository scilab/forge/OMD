function X = lhsample(n, d)
// Latin hypercube sample
// n: number of points
// d: space dimension
  X = (grand(d, 'prm', (0 : (n - 1))') + grand(n, d, 'def')) / n
endfunction
