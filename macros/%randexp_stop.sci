function out = %randexp_stop(this)
  if (this.nb_anal>=this.max_nb_anal) then
    out = %T;
  else
    out = %F;
  end
endfunction

