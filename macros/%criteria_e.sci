function out = %criteria_e(fieldname, num, this)

  // 1. fieldname = objective, constraint ou econstraint
  
  if find(fieldname == ['objective', 'constraint', 'econstraint']) ...
	~= [] then

    tmp = getfield(fieldname, this)
    out = tmp(num)
    
  // 2. fieldname est un gradient
  
  elseif find(fieldname == ['grad_objective', 'grad_constraint', ...
		    'grad_econstraint']) ~= [] then

    // on cherche s'il y a la liste des gradients...
    listname = 'list_' + fieldname
    allf = getfield(1, this)
    if or(allf(2:$) == listname) then
      // ... oui : on r�cup�re l'indice du gradient dans cette liste
      l = getfield(listname, this)
      i = find(l == num)
    else
      // ... non : on suppose que tous les gradients sont calcul�s, 
      // l'indice est donc donn� par le num�ro
      i = num
    end
    
    // extraction
    tmp = getfield(fieldname, this)
    out = tmp(i,:)
  
  end

endfunction
