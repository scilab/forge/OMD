	subroutine coef_pol(aw,phi0w,xmach,xk1)
	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
	f0 = phi0w*pi/180.d0
c
	if(xmach.le.1.d0) then
	  xk1 = 1.d0/pi/aw
	else
	  ac = 1.d0/2.d0/sqrt(xmach**2-1.d0)
	  if(aw.le.ac) then
	    ac = 1.d0/2.d0/sqrt(xmach**2-1.d0)*1.01
	    xk1 = ac*(xmach**2-1.d0)/
     &            (4.d0*ac*sqrt(xmach**2-1.d0)-2.d0)*cos(f0)
	  else
	    xk1 = aw*(xmach**2-1.d0)/
     &            (4.d0*aw*sqrt(xmach**2-1.d0)-2.d0)*cos(f0)
          endif
	endif
c
	return
	end
