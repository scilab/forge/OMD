	subroutine fus_weight(Df,xlfus,sfus,ZFW,cr,w_wing,w_wprop,z,w_fus)
	implicit real*8(a-h,o-z)
c
	xnu = 4.5d0
c
	call atmo(z,p,rho,T,sig)
c
	zcab = 2000.d0
	call atmo(zcab,pcab,rhoc,Tc,sig)
c
	Dp = abs(Pcab - P)
c
	W = ZFW - w_wing - w_wprop
	xl = xlfus - 0.5d0*cr
c
	Tip = 1.03d-4*Dp*Df
	Tib = 1.28d-4*xnu*w*xl/df**2
c
	if(tip.gt.tib) then
	  tif = tip
	else
	  tif = (tip**2 + tib**2) / 2.d0/tib
	endif
c
	w_fus = (5.13d0 + 0.418d0*tif)*Sfus
c
	return
	end

