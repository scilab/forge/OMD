	subroutine wing_weight(bw,t_c,xl,phi25,sext,TOW,ZFW,w_wing)
	implicit real*8 (a-h,o-z)
c
	pi = 4.d0*atan(1.d0)
	xnu = 4.5d0
c
	w_wing = 20.61*Sext + 
     &           5.39d-6*xnu*bw**3*sqrt(TOW*ZFW)*(1.d0+2.d0*xl)/
     &           (t_c*cos(phi25*pi/180.d0)**2*Sext*(1.d0+xl))
c
	return
	end
