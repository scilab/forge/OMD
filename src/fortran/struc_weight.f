	subroutine struc_weight(crw,bw,t_cw,xlw,phi25w,sextw,TOW,Wfuel,
     &                   	bt,t_ct,xlt,phi25t,sextt,
     &                          Df,xlfus,sfus,w_wprop,z,w_struc)
	implicit real*8 (a-h,o-z)
c
	ZFW = TOW - Wfuel
c
	if(zfw .le. 0.d0) then
c  print*,'PB ZFW = ',ZFW
c  print*,'TOW = ',TOW
c  print*,'Wfuel = ',Wfuel
	  zfw = 1.2*wfuel
c	  zfw = (1.2 + 0.5*rand())*wfuel
	endif
cprint*,'tow = ',tow
cprint*,'bw = ',bw
cprint*,'t_cw = ',t_cw
cprint*,'xlw = ',xlw
cprint*,'phi25w = ',phi25w
cprint*,'sextw = ',sextw
cprint*,'bt = ',bt
cprint*,'t_ct = ',t_ct
cprint*,'xlt = ',xlt
cprint*,'phi25t = ',phi25t
cprint*,'sextt = ',sextt
c
	call wing_weight(bw,t_cw,xlw,phi25w,sextw,TOW,ZFW,w_wing)
	call tail_weight(bt,t_ct,xlt,phi25t,sextt,TOW,w_tail)
	call fus_weight(Df,xlfus,sfus,ZFW,crw,w_wing,w_wprop,z,w_fus)
c
cprint*,'w_wing = ',w_wing
cprint*,'w_tail = ',w_tail
cprint*,'w_fus  = ',w_fus
c
	w_struc = w_wing + w_tail + w_fus
c
	return
	end
c
