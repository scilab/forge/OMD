	subroutine dfsol(n,x,itmax,feval,xt,yt,y0,y,d)
	implicit real*8 (a-h,o-z)
	dimension x(*),xt(*),y0(*),y(*),d(*),yt(*)
	dimension f(100),g(100)
	common /nbtrak/nbt
	common /prsol/iprsol,icvg
	external balance_feval
c
	nbt = 0
c
	neq = 2
	sigmin = 1.d-10
	sigmax = 1.d5
	taumin = 0.1d0
	taumax = 0.5d0
	M = 10
	gam = 1.d-4
	eps = 1.d-8
	eps = 1.d-12
c
	m = 1
	call balance_feval(n,x,y0)
c
	f0 = 0.d0
	do i=1,n
	  f0 = f0 + y0(i)**2
	enddo
c
cprint*,'erreur initiale = ',f0
c
	errmax = eps*(1+f0)
	errmax = eps
c
	errmax = 10.
c
	sigma = 1.
	sigma = 2*sigmin
	fk    = f0
c
	lmax = 0
c
	do i=1,m
	  f(i) = 0.d0
	enddo
c
	do k=1,itmax
	  eta = f0 / (1.d0 + dble(k))**2
c
c... Step 1
c
	  if(k.le.m) then
	    lmax = lmax + 1
	    f(lmax) = fk
          else
	    do i=1,m-1
	      g(i) = f(i+1) 
	    enddo
	    do i=1,m-1
	      f(i) = g(i)
	    enddo
	    f(m) = fk
	  endif
	  fb = -1.d10
c  print*,'lmax = ',lmax
	  do i=1,m
	    fb = max(fb,f(i))
	  enddo
c  print*,'fb = ',fb
	  do i=1,n
	    d(i) = -sigma*y0(i)
	  enddo
c
	  alm = 1.d0
	  alp = 1.d0
c
c... Step 2
c 
2	  continue
	  do i=1,n
	    xt(i) = x(i) + alp*d(i)
	  enddo
	  call balance_feval(n,xt,yt)
	  ft1 = 0.d0
	  do i=1,n
	    ft1 = ft1 + yt(i)**2
	  enddo
	  borne1 = fb + eta - gam*alp**2*fk
	  do i=1,n
	    xt(i) = x(i) - alm*d(i)
	  enddo
	  call balance_feval(n,xt,yt)
	  ft2 = 0.d0
	  do i=1,n
	    ft2 = ft2 + yt(i)**2
	  enddo
	  borne2 = fb + eta - gam*alm**2*fk
c
	  if(ft1.le.borne1) then
	    do i=1,n
	      xt(i) = x(i) + alp*d(i)
	    enddo
	  else if(ft2.le.borne2) then
	    do i=1,n
	      xt(i) = x(i) - alm*d(i)
	    enddo
	  else
	    nbt = nbt + 1
c    print*,'altp n = ',alp**2*fk
c    print*,'atlp d = ',(ft1 + (2.d0*alp-1.d0)*fk)
c    print*,'altm n = ',alm**2*fk
c    print*,'atlm d = ',(ft2 + (2.d0*alm-1.d0)*fk)
c    pause
c
	    altp = alp**2*fk/(ft1 + (2.d0*alp-1.d0)*fk)
	    altm = alm**2*fk/(ft2 + (2.d0*alm-1.d0)*fk)
	    if(altp.lt.taumin*alp) then
	      alp = taumin*alp
	    elseif(altp.gt.taumax*alp) then
	      alp = taumax*alp
	    else
	      alp = altp
	    endif
	    if(altm.lt.taumin*alm) then
	      alm = taumin*alm
	    elseif(altp.gt.taumax*alp) then
	      alm = taumax*alm
	    else
	      alm = altm
	    endif
c    print*,'alm = ',alm,' alp = ',alp
	    goto 2
	  endif
c
c... Step 3
c
	  call balance_feval(n,xt,y)
	  fk = 0.d0
	  do i=1,n
	    fk = fk + y(i)**2
	  enddo
	  sn = 0.d0
	  sd = 0.d0
	  do i=1,n
	    sn = sn + (xt(i)-x(i))**2 
c    sd = sd + (y(i) - y0(i))**2
	    sd = sd + (xt(i)-x(i))*(y(i) - y0(i))
	  enddo
	  do i=1,n
	    x(i) = xt(i) 
	    y0(i) = y(i)
	  enddo
c  print*,'it = ',k,' erreur = ',fk,' er0 = ',f0
	  if(iprsol.eq.1) then
	  print*,'it = ',k,' erreur = ',fk,' err0 = ',f0
	  endif
c  pause
	  if(fk.le.errmax)  then
c    print*,'it = ',k,' erreur = ',fk,' er0 = ',f0
	    icvg = 1
	    return
	  endif
	  sigma = sn / sd
	  if(abs(sigma).lt.sigmin .or. abs(sigma).gt.sigmax) then
	    if(fk.gt.1) then
	      sigma = 1.d0
	    elseif(1.d-5.le.fk .and. fk.le.1.d0) then
	      sigma = 1.d0/fk
	    else
	      sigma = 1.d5
	    endif
	  endif
c  print*,'it = ',k,' fk = ',fk,' sigma = ',sigma
c  print*,'sn = ',sn,' sd = ',sd
	enddo
	print*,'it = ',k,' fk = ',fk,' er0 = ',f0
	print*,'ALGO pas converge',' erreur = ',fk
	icvg = 0
	end
