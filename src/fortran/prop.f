	subroutine prop(z,xmach,F_F0,cs)
	implicit real*8 (a-h,o-z)
c
	call atmo(z,p,rho,T,sig)
c
	F_F0=(0.88d0+0.245d0*abs(xmach-0.6d0)**1.4)*sig**0.7
c
	Cs = 2.22d-5*(1.d0+0.35d0*xmach)*sqrt(T/288.15d0)
c
	return
	end

