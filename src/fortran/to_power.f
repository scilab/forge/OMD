	subroutine to_power(TOW,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,alpha,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,F0,
     &            Fto)
c----------------------------------------------------------------------
        implicit real*8 (a-h,o-z)
c
	neng = 2
c
c... Take off power
c
	call vstall(S,TOW,alpha,phi0w,aw,vs)
c
	igear = 1
	z0 = 0.d0
	call atmo(z0,p0,rho0,T0,sig)
	c0 = sqrt(1.4d0*p0/rho0)
c
	vto = 1.2d0*vs
	xmach0 =  vto / c0
	clto = TOW*9.81/(0.5d0*rho0*S*vto**2)
c
	call aero(z0,xmach0,TOW,S,phi0w,phi100w,t_cw,aw,bw,crw,
     &            cmaw,sextw,
     &            phi0t,phi100t,t_ct,bt,crt,cmat,sextt,
     &            dfus,xlfus,sfus,deng,xlnac,igear,cl,cd,drag)
c
	gc = asin(((neng-1)*F0-Drag)/(9.81d0*TOW))
c
	if(neng.eq.2) then
	  gmin = 0.024
	else
	  gmin = 0.027
	endif
c
	if(gc.le.gmin) then
	  Fto = (sin(gmin)*9.81d0*TOW+Drag)/(neng-1)
	else
	  Fto = F0
	endif
c
	return
	end
	
