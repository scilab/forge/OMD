	subroutine approach(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,xfact,vref)
c----------------------------------------------------------------------
        implicit real*8 (a-h,o-z)
c
	call balance(z,xmach,
     &    S,phi0w,phi100w,xlw,t_cw,
     &    phi0t,phi100t,xlt,t_ct,
     &    dfus,Wfuel,alpha,TOW,F0)

c
c... Geometry
c
        call wing_geom(S,phi0w,phi100w,xlw,dfus,
     &   aw,bw,crw,cmaw,campw,phi25w,sextw)
        call tail_geom(S,phi0t,phi100t,xlt,
     &   at,bt,crt,cmat,phi25t,sextt)
        call fus_geom(dfus,xmach,wfuel,xlfus,Sfus)
	call eng_geom(F0,deng,xleng)
	call nac_geom(deng,xleng,dnac,xlnac)
c
	z0 = 0.d0
	call atmo(z0,p0,rho0,t0,sig)
	c0 = sqrt(1.4d0*P0/Rho0)
c
	weight = xfact*TOW
c
	call vstall(S,weight,alpha,phi0w,aw,vs)
c
	vref = 1.3d0*vs
c
	return
	end
c
