#include "stack-c.h"

extern int C2F(tail_geom)(double*, double*, double*, double*, double*, double*, double*, double*, double*, double*);

int int_tail_geom(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int m3, n3, l3;
  int m4, n4, l4;
  int l5, l6, l7, l8, l9, l10;

  CheckLhs(6, 6);
  CheckRhs(4, 4);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(3, "d", &m3, &n3, &l3);
  if (m3 != 1 || n3 != 1) {cerro("Argument 3: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(4, "d", &m4, &n4, &l4);
  if (m4 != 1 || n4 != 1) {cerro("Argument 4: wrong type, expecting a scalar"); return 0;}

  CreateVar(5, "d", &m1, &n1, &l5);
  CreateVar(6, "d", &m1, &n1, &l6);
  CreateVar(7, "d", &m1, &n1, &l7);
  CreateVar(8, "d", &m1, &n1, &l8);
  CreateVar(9, "d", &m1, &n1, &l9);
  CreateVar(10, "d", &m1, &n1, &l10);

  C2F(tail_geom)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5), stk(l6), stk(l7), stk(l8), stk(l9), stk(l10));

  LhsVar(1) = 5;
  LhsVar(2) = 6;
  LhsVar(3) = 7;
  LhsVar(4) = 8;
  LhsVar(5) = 9;
  LhsVar(6) = 10;

  return 0;
}
