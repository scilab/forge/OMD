#include "stack-c.h"

extern int C2F(eng_geom)(double*, double*, double*);

int int_eng_geom(char* fname) 
{
  int m1, n1, l1;
  int l2, l3;

  CheckLhs(2, 2);
  CheckRhs(1, 1);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}

  CreateVar(2, "d", &m1, &n1, &l2);
  CreateVar(3, "d", &m1, &n1, &l3);

  C2F(eng_geom)(stk(l1), stk(l2), stk(l3));

  LhsVar(1) = 2;
  LhsVar(2) = 3;

  return 0; 
}
