#include "stack-c.h"

extern int C2F(approach)(double*, double*, double*, double*, double*, double*, double*, 
			 double*, double*, double*, double*, double*, double*, double*, 
			 double*, double*);

int int_approach(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int m3, n3, l3;
  int m4, n4, l4;
  int m5, n5, l5;
  int m6, n6, l6;
  int m7, n7, l7;
  int m8, n8, l8;
  int m9, n9, l9;
  int m10, n10, l10;
  int m11, n11, l11;
  int m12, n12, l12;
  int m13, n13, l13;
  int m14, n14, l14;
  int m15, n15, l15;
  int l16;

  CheckLhs(1, 1);
  CheckRhs(15, 15);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(3, "d", &m3, &n3, &l3);
  if (m3 != 1 || n3 != 1) {cerro("Argument 3: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(4, "d", &m4, &n4, &l4);
  if (m4 != 1 || n4 != 1) {cerro("Argument 4: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(5, "d", &m5, &n5, &l5);
  if (m5 != 1 || n5 != 1) {cerro("Argument 5: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(6, "d", &m6, &n6, &l6);
  if (m6 != 1 || n6 != 1) {cerro("Argument 6: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(7, "d", &m7, &n7, &l7);
  if (m7 != 1 || n7 != 1) {cerro("Argument 7: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(8, "d", &m8, &n8, &l8);
  if (m8 != 1 || n8 != 1) {cerro("Argument 8: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(9, "d", &m9, &n9, &l9);
  if (m9 != 1 || n9 != 1) {cerro("Argument 9: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(10, "d", &m10, &n10, &l10);
  if (m10 != 1 || n10 != 1) {cerro("Argument 10: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(11, "d", &m11, &n11, &l11);
  if (m11 != 1 || n11 != 1) {cerro("Argument 11: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(12, "d", &m12, &n12, &l12);
  if (m12 != 1 || n12 != 1) {cerro("Argument 12: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(13, "d", &m13, &n13, &l13);
  if (m13 != 1 || n13 != 1) {cerro("Argument 13: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(14, "d", &m14, &n14, &l14);
  if (m14 != 1 || n14 != 1) {cerro("Argument 14: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(15, "d", &m15, &n15, &l15);
  if (m15 != 1 || n15 != 1) {cerro("Argument 15: wrong type, expecting a scalar"); return 0;}

  CreateVar(16, "d", &m1, &n1, &l16);

  C2F(approach)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5), stk(l6), 
		stk(l7), stk(l8), stk(l9), stk(l10), stk(l11), stk(l12), 
		stk(l13), stk(l14), stk(l15), stk(l16));

  LhsVar(1) = 16;

  return 0;
}
