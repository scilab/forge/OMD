#include "stack-c.h"

extern int C2F(vstall)(double*, double*, double*, double*, double*, double*);

int int_vstall(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int m3, n3, l3;
  int m4, n4, l4;
  int m5, n5, l5;
  int l6;

  CheckLhs(1, 1);
  CheckRhs(5, 5);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar:"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(3, "d", &m3, &n3, &l3);
  if (m3 != 1 || n3 != 1) {cerro("Argument 3: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(4, "d", &m4, &n4, &l4);
  if (m4 != 1 || n4 != 1) {cerro("Argument 4: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(5, "d", &m5, &n5, &l5);
  if (m5 != 1 || n5 != 1) {cerro("Argument 5: wrong type, expecting a scalar"); return 0;}

  CreateVar(6, "d", &m1, &n1, &l6);

  C2F(vstall)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5), stk(l6));

  LhsVar(1) = 6;

  return 0;
}
