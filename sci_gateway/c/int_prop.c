#include "stack-c.h"

extern int C2F(prop)(double*, double*, double*, double*);

int int_prop(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int l3, l4;

  CheckLhs(2, 2);
  CheckRhs(2, 2);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}

  CreateVar(3, "d", &m1, &n1, &l3);
  CreateVar(4, "d", &m1, &n1, &l4);

  C2F(prop)(stk(l1), stk(l2), stk(l3), stk(l4));

  LhsVar(1) = 3;
  LhsVar(2) = 4;

  return 0; 
}
