#include "stack-c.h"

extern int C2F(aero)(double*, double*, double*, double*, double*, double*, double*, double*, 
		     double*, double*, double*, double*, double*, double*, double*, double*, 
		     double*, double*, double*, double*, double*, double*, double*, double*, 
		     double*, double*, double*, double*);

int int_aero(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int m3, n3, l3;
  int m4, n4, l4;
  int m5, n5, l5;
  int m6, n6, l6;
  int m7, n7, l7;
  int m8, n8, l8;
  int m9, n9, l9;
  int m10, n10, l10;
  int m11, n11, l11;
  int m12, n12, l12;
  int m13, n13, l13;
  int m14, n14, l14;
  int m15, n15, l15;
  int m16, n16, l16;
  int m17, n17, l17;
  int m18, n18, l18;
  int m19, n19, l19;
  int m20, n20, l20;
  int m21, n21, l21;
  int m22, n22, l22;
  int m23, n23, l23;
  int m24, n24, l24;
  int m25, n25, l25;
  int l26, l27, l28;

  CheckLhs(3, 3);
  CheckRhs(25, 25);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(3, "d", &m3, &n3, &l3);
  if (m3 != 1 || n3 != 1) {cerro("Argument 3: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(4, "d", &m4, &n4, &l4);
  if (m4 != 1 || n4 != 1) {cerro("Argument 4: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(5, "d", &m5, &n5, &l5);
  if (m5 != 1 || n5 != 1) {cerro("Argument 5: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(6, "d", &m6, &n6, &l6);
  if (m6 != 1 || n6 != 1) {cerro("Argument 6: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(7, "d", &m7, &n7, &l7);
  if (m7 != 1 || n7 != 1) {cerro("Argument 7: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(8, "d", &m8, &n8, &l8);
  if (m8 != 1 || n8 != 1) {cerro("Argument 8: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(9, "d", &m9, &n9, &l9);
  if (m9 != 1 || n9 != 1) {cerro("Argument 9: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(10, "d", &m10, &n10, &l10);
  if (m10 != 1 || n10 != 1) {cerro("Argument 10: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(11, "d", &m11, &n11, &l11);
  if (m11 != 1 || n11 != 1) {cerro("Argument 11: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(12, "d", &m12, &n12, &l12);
  if (m12 != 1 || n12 != 1) {cerro("Argument 12: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(13, "d", &m13, &n13, &l13);
  if (m13 != 1 || n13 != 1) {cerro("Argument 13: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(14, "d", &m14, &n14, &l14);
  if (m14 != 1 || n14 != 1) {cerro("Argument 14: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(15, "d", &m15, &n15, &l15);
  if (m15 != 1 || n15 != 1) {cerro("Argument 15: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(16, "d", &m16, &n16, &l16);
  if (m16 != 1 || n16 != 1) {cerro("Argument 16: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(17, "d", &m17, &n17, &l17);
  if (m17 != 1 || n17 != 1) {cerro("Argument 17: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(18, "d", &m18, &n18, &l18);
  if (m18 != 1 || n18 != 1) {cerro("Argument 18: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(19, "d", &m19, &n19, &l19);
  if (m19 != 1 || n19 != 1) {cerro("Argument 19: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(20, "d", &m20, &n20, &l20);
  if (m20 != 1 || n20 != 1) {cerro("Argument 20: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(21, "d", &m21, &n21, &l21);
  if (m21 != 1 || n21 != 1) {cerro("Argument 21: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(22, "d", &m22, &n22, &l22);
  if (m22 != 1 || n22 != 1) {cerro("Argument 22: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(23, "d", &m23, &n23, &l23);
  if (m23 != 1 || n23 != 1) {cerro("Argument 23: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(24, "d", &m24, &n24, &l24);
  if (m24 != 1 || n24 != 1) {cerro("Argument 24: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(25, "d", &m25, &n25, &l25);
  if (m25 != 1 || n25 != 1) {cerro("Argument 25: wrong type, expecting a scalar"); return 0;}

  CreateVar(26, "d", &m1, &n1, &l26);
  CreateVar(27, "d", &m1, &n1, &l27);
  CreateVar(28, "d", &m1, &n1, &l28);

  C2F(aero)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5), stk(l6), stk(l7), stk(l8), 
	    stk(l9), stk(l10), stk(l11), stk(l12), stk(l13), stk(l14), stk(l15), stk(l16), 
	    stk(l17), stk(l18), stk(l19), stk(l20), stk(l21), stk(l22), stk(l23), stk(l24), 
	    stk(l25), stk(l26), stk(l27), stk(l28));

  LhsVar(1) = 26;
  LhsVar(2) = 27;
  LhsVar(3) = 28;

  return 0;
}
