#include "stack-c.h"

extern int C2F(fus_geom)(double*, double*, double*, double*, double*);

int int_fus_geom(char* fname) 
{
  int m1, n1, l1;
  int m2, n2, l2;
  int m3, n3, l3;
  int l4, l5;

  CheckLhs(2, 2);
  CheckRhs(3, 3);

  GetRhsVar(1, "d", &m1, &n1, &l1);
  if (m1 != 1 || n1 != 1) {cerro("Argument 1: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(2, "d", &m2, &n2, &l2);
  if (m2 != 1 || n2 != 1) {cerro("Argument 2: wrong type, expecting a scalar"); return 0;}
  GetRhsVar(3, "d", &m3, &n3, &l3);
  if (m3 != 1 || n3 != 1) {cerro("Argument 3: wrong type, expecting a scalar"); return 0;}

  CreateVar(4, "d", &m1, &n1, &l4);
  CreateVar(5, "d", &m1, &n1, &l5);

  C2F(fus_geom)(stk(l1), stk(l2), stk(l3), stk(l4), stk(l5));

  LhsVar(1) = 4;
  LhsVar(2) = 5;

  return 0;
}
