<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="optimizer" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>2-Jul-2007</pubdate>
  </info>

  <refnamediv>
    <refname>optimizer</refname>

    <refpurpose>optimizer standardization</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>Unlike traditional optimization functions (such as
    <literal>optim</literal>), an optimizer is an object (i.e. a
    <literal>mlist</literal>) which is decoupled from the simulator. It is
    controled with 4 methods:</para>

    <variablelist>
      <varlistentry>
        <term>stop</term>

        <listitem>
          <para>the stopping condition</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ask</term>

        <listitem>
          <para>asks an optimizer the points to simulate</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>tell</term>

        <listitem>
          <para>tells an optimizer the results of the simulations</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>best</term>

        <listitem>
          <para>returns the best known point</para>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>With these 4 methods, the generic optimization loop could be</para>

    <programlisting> 
while ~stop(OPT)
  X = ask(OPT)
  y = SIM(X)
  tell(OPT,X,y)
end
[yopt, xopt] = best(OPT)
 </programlisting>

    <para>(where <literal>OPT</literal> is the optimizer and
    <literal>SIM</literal> the simulator), also represented by the
    scheme</para>

    <mediaobject>
      <imageobject>
        <imagedata align="center" fileref="../../images/optimizer.gif" />
      </imageobject>
    </mediaobject>
  </refsection>

  <refsection>
    <title>Overloading</title>

    <para><literal>stop</literal>, <literal>ask</literal>,
    <literal>tell</literal> and <literal>best</literal> are generic functions
    wich must be overloaded (see overloading) for each type of optimizer, with
    the syntax:</para>

    <programlisting> 
out = %&lt;optimizer_type&gt;_stop(this)
X = %&lt;optimizer_type&gt;_ask(this)
this = %&lt;optimizer_type&gt;_tell(this, X, y)
[yopt, xopt] = %&lt;optimizer_type&gt;_best(this)
 </programlisting>

    <para>where <literal>&lt;optimizer_type&gt;</literal> must be replaced by
    the type of the optimizer (i.e. the string that will be returned by
    <literal>typeof(OPT)</literal>), and the arguments in these declarations
    are</para>

    <variablelist>
      <varlistentry>
        <term>this</term>

        <listitem>
          <para>(<literal>mlist</literal>) : the optimizer object;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>X</term>

        <listitem>
          <para>(matrix of size <literal>n*d</literal>) : the requested points
          (<literal>n</literal> is the number of points, and
          <literal>d</literal> the space dimension); if
          <literal>n=1</literal>, the optimizer is called
          "one-point-optimizer";</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>y</term>

        <listitem>
          <para>response values; the type of <literal>y</literal> is not
          necessary <literal>"criteria"</literal> (see <link
          linkend="simulator">simulator</link>), this choice is up to the
          programmer of the optimizer;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>out</term>

        <listitem>
          <para>(boolean) : stopping condition;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>yopt</term>

        <listitem>
          <para>known best value of <literal>y</literal>;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>xopt</term>

        <listitem>
          <para>value of <literal>x</literal> giving
          <literal>yopt</literal>.</para>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>To be complete, an optimizer should also have a constructor, i.e. a
    function which initialize the optimizer object with parameters given by
    the user.</para>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="simulator">simulator</link></member>

      <member><link linkend="metamodel">metamodel</link></member>

      <!--<member>
      <link linkend="overloading">overloading</link>
    </member>-->
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <variablelist>
      <varlistentry>
        <term>G. Pujol</term>

        <listitem>
          <para>ENSM-SE</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>Pujol G., Le Riche R. (2007). Description de l'architecture Scilab
    pour le projet RNTL/OMD.</para>
  </refsection>
</refentry>
