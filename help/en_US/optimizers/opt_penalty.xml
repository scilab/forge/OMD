<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="opt_penalty" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>29-Nov-2007</pubdate>
  </info>

  <refnamediv>
    <refname>opt_penalty</refname>

    <refpurpose>penalized criterion for constrained optimization</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>fp = opt_penalty(f, g, alpha [, p])
[fp, gradfp] = opt_penalty(f, gradf, g, gradg, alpha [, p])</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>f</term>

        <listitem>
          <para>(scalar) : value of the objective function;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>gradf</term>

        <listitem>
          <para>(vector of length <literal>d</literal>) : value of the
          gradient of the objective function;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>g</term>

        <listitem>
          <para>(vector of length <literal>q</literal>) : value of the
          constraint functions;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>gradg</term>

        <listitem>
          <para>(matrix of size <literal>q*d</literal>) : value of the
          gradient of the constraint functions;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>alpha</term>

        <listitem>
          <para>(scalar) : penalty coefficient;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>p</term>

        <listitem>
          <para>(function) : penalty function;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>fp</term>

        <listitem>
          <para>(scalar) : value of the penalized criterion;</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>gradfp</term>

        <listitem>
          <para>(vector of length <literal>d</literal>) : value of the
          gradient of the penalized criterion.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>The constrained optimization problem is</para>

    <programlisting>min f(x) w.r.t. x, so that g(x) &lt;= 0</programlisting>

    <para>Given the value of <literal>f(x)</literal> and
    <literal>g(x)</literal>, <literal>opt_penalty</literal> returns the value
    of the penalized criterion</para>

    <mediaobject>
      <imageobject>
        <imagedata fileref="../../images/opt_penalty.gif" />
      </imageobject>
    </mediaobject>

    <para>The value of the gradient of <literal>fp</literal>,
    <literal>gradfp</literal>, is also returned if the values of the gradients
    <literal>gradf</literal> and <literal>gradg</literal> are given.</para>

    <para>The penalty functions are defined by a single Scilab function
    <literal>p</literal>, as <literal>[z,dz]=p(y)</literal> where
    <literal>y</literal> and <literal>z</literal> are vectors of length
    <literal>q</literal>, and <literal>dz</literal> is the vector of the
    derivatives with respect to <literal>y</literal>. The derivatives are
    requested only when the gradients <literal>gradf</literal> and
    <literal>gradg</literal> are given.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"> 
// Penalty function: p(g(x)) = max(g(x),0)^2
function [z, dz] = p(y)
  z = max(y,0).^2
  dz = 2*y.*(y&gt;=0)
endfunction

// Cost function
function [fp, gradfp, ind] = costf(x, ind)
  global alpha 
  f = 2*x   // objective: f(x) = 2x
  gradf = 2*ones(x)
  g = 1-x   // constraint: (1-x) &lt;= 0
  gradg = -ones(x)
  [fp, gradfp] = opt_penalty(f, gradf, g, gradg, alpha, p)
  alpha = 1.1 * alpha // alpha is increased while optimizing
endfunction

global alpha;
alpha = 1;
[fopt, xopt] = optim(costf, 0)
  </programlisting>
  </refsection>

  <!--<refsection><title>See Also</title><simplelist type="inline">
    <member> <link linkend="optim">optim</link> </member>
  </simplelist></refsection>-->

  <refsection>
    <title>Authors</title>

    <variablelist>
      <varlistentry>
        <term>G. Pujol</term>

        <listitem>
          <para>ENSM-SE</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
</refentry>
