#!/bin/bash
# eq2gif.bash foo.tex
# -> generates foo.gif
INPUTFILE=`basename $1 .tex`
latex $INPUTFILE
dvips $INPUTFILE.dvi -o
ps2eps $INPUTFILE.ps
convert -density 150 $INPUTFILE.eps $INPUTFILE.gif
rm $INPUTFILE.{log,aux,dvi,ps,eps}
