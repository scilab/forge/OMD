function [x]=decas(t,p)
  n=length(t);
  [m,dim]=size(p);
	 
  x=zeros(n,dim);

  for k=1:n
    s=t(k);
    pc=p;
    for i=1:m-1
      for j=1:m-i
        pc(j,1:dim) = (1-s)*pc(j,1:dim) + s*pc(j+1,1:dim);
      end
    end
    x(k,1:dim) = pc(1,1:dim);
  end
endfunction
