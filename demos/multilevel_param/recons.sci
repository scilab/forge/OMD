function [J] = recons(A,y,yt)

J = .5*(y-yt)'*A*(y-yt);

endfunction

function [G] = grecons(A,y,yt)

G = A*(y-yt);

endfunction
