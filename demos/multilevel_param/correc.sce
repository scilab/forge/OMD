global A E Q x xtarget nbit yz opt
if (yz=='y') then
  Q = Z*Z';
elseif (yz=='z') then
  P = jmat(n-1,1);
  Q = ZU*P*ZU';
end

Ay = E'*Q*A*Q*E;
y  = zeros(m+1,1);

if (opt=='qn') then
  nbit = 0;
  [Jo,yo,Go]=optim(ccost,y,'qn','ar',nbcorr,100,1e-8);

  y = yo;
  J = [J Jo];
  G = grecons(A,x+Q*E*y,xtarget);
elseif (opt=='sd') then
  ys = y;
  Dy = spec(Ay);
  rho = 1/max(abs(Dy));
//  rho = 1.5e1;
  for i=1:nbcorr
    Gy = E'*Q*G;
    y  = y - rho*Gy;
    ys = [ys y];
    J  = [J recons(A,x+Q*E*y,xtarget)];
    G  = grecons(A,x+Q*E*y,xtarget);
  end
end

// solve exactly
//by = E'*Q*A*(xtarget-x);
//yd = Ay\by;
//Jd  = recons(A,x+Q*E*yd,xtarget);
//Gd  = grecons(A,x+Q*E*yd,xtarget);
//y = yd;
//J = [J Jd];
//G = Gd;

// return value
x  = x + Q*E*y;

