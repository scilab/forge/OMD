function [C]=cnk(n)
  C=eye(n+1,n+1);
  C(:,1)=ones(n+1,1);
  for i=2:n
    C(i+1,2:i)=C(i,1:i-1)+C(i,2:i);
  end
endfunction
