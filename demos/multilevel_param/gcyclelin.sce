//------- Demonstrator for Hierarchical -------//
//------- Shape Optimization Methods ----------//

//-------- INRIA Sophia-Antipolis ------------//
//--------- OPALE Project-Team ---------------//
//--------- September 2007    ---------------//

lines(0);

global A E Q x xtarget nbit yz opt

//--- PARAMETERS ------------------------------//
//
//  TO BE EDITED
//


m = 15;   // coarse degree
n = 20;   // fine degree

nt = 20;  // target degree <= n
xtarget = [0.0;
    1.7839563  ;
  - 1.6799047  ;
  - 4.7412901  ;
    0.0685344  ;
    0.2329764  ;
    0.5969476  ;
    0.617307   ;
  - 0.3182400  ;
    2.7945467  ;
    2.9010718  ;
    4.8085421  ;
    3.1870661  ;
  - 0.7431276  ;
  - 2.5384394  ;
    4.2295325  ;
  - 3.9992542  ;
  - 0.3217819  ;
  - 1.0495023  ;
  - 4.6338829;
    0.0];

nbsl   = 200;  // nb of iter. for single level
nbml_f = 5;    // ----------- on fine level
nbml_c = 5;    // ----------- on coarse level
nbv    = 15;   // nb of cycles

yz  = 'z';  // y | z 

opt = 'sd'; // qn | sd

//---------------------------------------------//

// load elementary functions
exec('recons.sci');
exec('ccost.sci');
exec('fcost.sci');
exec('cnk.sci');
exec('decas.sci');
exec('degelev1d.sci');
exec('degelevm1d.sci');

// compute A and its projected spectrum [ZU,D]
// set support variables xs (FFD param) and t (Bezier curve)
exec('Absupp.sce');

// Elevation matrix for target
Ent = eye(nt+1,nt+1);
for k=nt:n-1
  a  = (1:k)/(k+1);
  Ek = diag([a,1],-1)+diag([1,1-a,0]);
  Ent  = Ek(:,1:k+1)*Ent;
end
xtarget = Ent*xtarget;
Btarget = decas(t,xtarget);

// Elevation matrix for vcycle
E = eye(m+1,m+1);
for k=m:n-1
  a  = (1:k)/(k+1);
  Ek = diag([a,1],-1)+diag([1,1-a,0]);
  E  = Ek(:,1:k+1)*E;
end

// initial curve
x0 = zeros(n+1,1);
B0 = decas(t,x0);
J0 = recons(A,x0,xtarget);
G0 = grecons(A,x0,xtarget);

//--- SINGLE LEVEL ----------------------------//

// steepest descent
nbf = nbsl;
nsl = nbf;

x = x0;
J = J0;
G = G0;
exec('finesteep.sce');

xsl = x;
Jsl = J;

// Quasi-Newton
x    = x0;
nbit = 0;
[Jqnf,xqnf,Gqnf] = optim(fcost, x,'qn','ar',nbf,100,1e-6);
nbqnf = nbit;

//--- VG-CYCLES -------------------------------//

// number of iterations
nbf    = nbml_f;
nbcorr = nbml_c;

// initial point
x = x0;
J = J0;
G = G0;
nbeval = 0;

// vcycle
nml = 0;
for v=1:nbv
  // -- fine -- //
  disp('cycle '+string(v))
  if (opt=='sd') then
    exec('finesteep.sce');
    nbeval = [nbeval , nml+1:nml+nbf];
    nml = nml+nbf;
  elseif (opt=='qn') then
    exec('fine.sce');
    nml = nml+nbit;
    nbeval = [nbeval , nml];
  end

  // -- coarse -- //
  exec('correc.sce');
  if (opt=='qn') then
    nml = nml+nbit;
    nbeval = [nbeval , nml];
  elseif (opt=='sd') then
    nbeval = [nbeval , nml+1:nml+nbcorr];
    nml = nml+nbcorr;
  end
end

nbf=0;
if (nbf>0) then
  if (opt=='sd') then
    exec('finesteep.sce');
    nbeval = [nbeval , nml+1:nml+nbf];
  elseif (opt=='qn') then
    exec('fine.sce');
    nml = nml+nbit;
    nbeval = [nbeval , nml];
  end
end

xml=x;
Jml=J;

//-------------------------------------//

// plot results
Bsl = decas(t,xsl);
Bml = decas(t,xml);

h = scf(1);
plot2d(t,[B0 Btarget Bsl Bml],style=[0 3 1 2])
legends(["initial" "target" "single-level" "multi-level"],[0 3 1 2],opt="ur");
xtitle("Bezier Curves","x","y")

h = scf(2);
plot2d(0:nbsl,Jsl,logflag="nl",style=1)
plot2d(nbeval,Jml,logflag="nl",style=2)
legends(["single-level" "multi-level"],[1 2],opt="ur");
if (opt=='qn')
  plot2d(0:nbqnf,Jqnf*ones(nbqnf+1,1),logflag="nl",style=0)
end
xtitle("Convergence history","it","cost function")

h = scf(3);
plot2d(t,[Btarget-Bsl Btarget-Bml])
legends(["single-level" "multi-level"],[1 2],opt="ur");
xtitle("Error curves","x","y")

