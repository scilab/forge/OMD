exec('./decomposition_POD.sci');

// Display mode
mode(0);

// Display warning for floating point exception
ieee(1);

A = rand(100,10);
[parametres,Base,P_moyen,critere_energetique] = decomposition_POD(A);

h = scf();
plot([1:length(critere_energetique)],1-critere_energetique);
xlabel("mode")
ylabel("POD reconstruction error")
