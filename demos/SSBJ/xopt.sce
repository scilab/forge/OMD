// Ce script permet de vérifier l'intégration Scilab des sources de
// SSBJ. On simule l'optimum présenté dans le rapport DA "Rapport
// d'avancement. Contrat ANR OMD" du 22/09/07. On retrouve bien les
// valeurs du tableau p. 30.
// Gilles Pujol, 24 mars 2008.

sim = ssbj();
sim.ract_min = 6500000;
sim.dist_max = 1828;
sim.vref_max = 70;

xopt = [15029.8219, 1.80419439, 100., 52.0242245, 10.9333309, 0.0631019786, ...
	0.0436489457, 68.0947767, 9.99479317, 0.443188622, 0.0503080707, ...
	2.26402943, 15000., 14.9882846, 0.868905052];

yopt = sim(xopt);
disp(yopt);
