// Implementation of the metamodel 'nearest'

function this = nearest(X, y, param)
  rhs = argn(2)
  if rhs == 2 then param = struct(), end
  this = mlist(['nearest', 'X', 'y'], X, y)
endfunction

function [y, v] = %nearest_e(x, this)
  // d is the vector of the (squared) distances from x to each point
  // of the design of experiments X
  d = []
  for i = 1:size(this.X, 1)
    d = [d, (sum((this.X(i,:) - x).^2))]
  end 
  // returns the value of the nearest point
  [val, i] = min(d)
  y = this.y(i)
  v = %nan // no error estimation
endfunction

// Test on the Branin function

function y = branin(x1, x2)
  y = ( x2 - (5.1 / (4 * %pi^2)) * x1.^2 + 5 * x1 / %pi - 6 ).^2 ...
      + 10 * (1 - 1 / (8 * %pi)) * cos(x1) + 10
endfunction

x_min = [-5, 0];
x_max = [10, 15];

// Generation of n random points...
n = 20;
X = [grand(n, 1, 'unf', x_min(1), x_max(1)), ...
     grand(n, 1, 'unf', x_min(2), x_max(2))];
// ... computation of the Branin function responses...
y = branin(X(:,1), X(:,2))';
// ... and estimation of the metamodel
met = nearest(X, y);

// Plot of the metamodel versus the real function
n  = 30;
x1 = linspace(x_min(1), x_max(1), n);
x2 = linspace(x_min(2), x_max(2), n);
theta = -50;
alpha = 7;
flag  = [2,1,4];
ebox  = [x_min(1), x_max(1), x_min(2), x_max(2), 0, 350];
subplot(1,2,1);
fplot3d(x1, x2, branin, theta=theta, alpha=alpha, flag=flag, ebox=ebox);
subplot(1,2,2);
deff('z=_met(x,y)', 'z=met([x,y])');
fplot3d(x1, x2, _met, theta=theta, alpha=alpha, flag=flag, ebox=ebox);
