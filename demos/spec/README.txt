This folder contains the examples of the Scilab specifications document for the OMD projet (deliverable 8.1)

* linreg.sce : linear regression metamodel (section 2, p 9-10)
* rsearch.sce : random search optimizer (section 4, p 15-16)
* nearest.sce : so-called 'nearest' metamodel (section 5, p 18)
* desc1d-v*.sce : files of the tutorial "How to program an ask & tell
  optimizer" (section 8, p 24-30)
* wrap_sim.sce : wrapping a simulator into a cost function (section 9.1, p 30-32)
* wrap_optim.sce : wrapping the optim function into an "ask & tell" optimizer (section 9.2, p 32-34)

