// Implementation of the optimizer 'rsearch'
// (Note that it is just the example of the specification document. The
// version implemented in this library is greatly improved.)

function this = rsearch(param)
  this = mlist(['rsearch', 'd', 'x_min', 'x_max', 'nb_max_iter', ...
		'iter', 'xopt', 'yopt'])  
  this.d = param.d
  this.x_min = getfield_deft('x_min', param, zeros(1, param.d))
  this.x_max = getfield_deft('x_max', param, ones(1, param.d))
  this.nb_max_iter = param.nb_max_iter
  this.iter = 0
  this.xopt = []
  this.yopt = %inf
endfunction

function out = %rsearch_stop(this)
  out = this.iter >= this.nb_max_iter
endfunction

function x = %rsearch_ask(this)
  x = (this.x_max - this.x_min) .* grand(1, this.d, 'def') + this.x_min
endfunction

function this = %rsearch_tell(this, x, y)
  this.iter = this.iter + 1
  if y < this.yopt then
    this.xopt = x
    this.yopt = y
  end
endfunction

function [yopt, xopt] = %rsearch_best(this)
  yopt = this.yopt
  xopt = this.xopt
endfunction


// Test on the Branin function


function y = branin(x1, x2)
  y = ( x2 - (5.1 / (4 * %pi^2)) * x1^2 + 5 * x1 / %pi - 6 )^2 ...
      + 10 * (1 - 1 / (8 * %pi)) * cos(x1) + 10
endfunction

x_min = [-5, 0];
x_max = [10, 15];

// contour lines of the Branin function
n  = 100;
x1 = linspace(x_min(1), x_max(1), n);
x2 = linspace(x_min(2), x_max(2), n);
scf();
xset('fpf', ' ');
nl = 50;
contour2d(x1, x2, branin, nl, style=color('gray')*ones(1,nl));

// initialization of the optimizer
opt_param = struct();
opt_param.d = 2;
opt_param.x_min = x_min;
opt_param.x_max = x_max;
opt_param.nb_max_iter = 10;
opt = rsearch(opt_param);

// optimization
while ~stop(opt)
  x = ask(opt);
  y = branin(x(1), x(2));
  opt = tell(opt, x, y);
  // display and plot at each iteration
  printf('iter=%i x1=%f x2=%f y=%f\n', opt.iter, x, y); 
  plot2d(x(1), x(2), style=-1, rect=[x_min, x_max]);
end

// display the optimum found
printf('\nOptimum:\n');
[yopt, xopt] = best(opt);
printf('x1           x2          y\n');
printf('%11f %11f %11f\n', xopt, yopt);
