// *** desc1d optimizer ***

// constructor
function this = desc1d(param)
  this = mlist(['desc1d', 'x0', 'epsilon', 'alpha', 'grad', ...
                'xcur', 'ycur', 'xlast', 'ylast', 'firstiter']);
  labels = getfield(1,param);
  for i = labels(3:$), this(i) = param(i), end
  this('xcur')      = this('x0');
  this('xlast')     = this('xcur');
  this('firstiter') = %t;
endfunction

// method 'stop'
function out = %desc1d_stop(this)
  out = abs(this.grad) < this.epsilon;
endfunction

// method 'ask'
function x = %desc1d_ask(this)
  if this.firstiter
    x = this.xcur;
  else
    x = this.xcur - this.alpha * this.grad;
  end
endfunction

//  method 'tell'
function this = %desc1d_tell(this, x, y)
  // saving values
  this.xcur = x;
  this.ycur = y;
  // updating variables
  if this.firstiter
    this.firstiter = %f;
  else
    this.grad = (this.ycur - this.ylast) / (this.xcur - this.xlast);
  end
  // i++
  this.xlast = this.xcur;
  this.ylast = this.ycur;
endfunction

//  method 'best'
function [yopt, xopt] = %desc1d_best(this)
  yopt = this.ycur
  xopt = this.xcur
endfunction

// *** main program ***

// function to optimize
function y = f(x)
  y = x^2
endfunction

// initializing the optimizer
param         = struct();
param.x0      = 5.;
param.epsilon = 1.E-2;
param.alpha   = 1.E-1;
param.grad    = 1;
optimizer     = desc1d(param);

// optimization loop
while ~stop(optimizer)
  x = ask(optimizer);
  y = f(x);
  optimizer = tell(optimizer, x, y);

  // output
  [yopt, xopt] = best(optimizer);
  printf('x = %f   y = %f\n', xopt, yopt);
end
