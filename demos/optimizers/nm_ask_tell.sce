////////////////////////////////////
// Definition of the test problem //
////////////////////////////////////

function Res = min_bd_branin()
Res = [-5 0]';
endfunction

function Res = max_bd_branin()
Res = [15 10]';
endfunction

function Res = opti_branin()
Res = [-%pi     12.275; ...
        %pi     12.275; ...
        9.42478  2.475]';
endfunction

function y = branin(x)
y = (x(2)-(5.1/(4*%pi^2))*x(1)^2+5/%pi*x(1)-6)^2+10*(1-1.0/(8*%pi))*cos(x(1))+10;
endfunction

ItMX = 100;

nmopt      = nmomd();
nmopt.ItMX = ItMX;
nmopt.kelley_restart  = %F;
nmopt.kelley_alpha    = 1e-4;	
nmopt.simplex_relsize = 0.1;
nmopt.log             = %F;

nmopt.upper = max_bd_branin();
nmopt.lower = min_bd_branin();

nmopt.x0   = (nmopt.upper - nmopt.lower).*rand(size(nmopt.upper,1),size(nmopt.upper,2)) + nmopt.lower;

y_min = [];
while ~stop(nmopt)
  printf('nmopt running: iteration %d / %d - ', ItMX - nmopt.ItMX + 1, ItMX);
  x = ask(nmopt);
  y = [];
  for i=1:size(x,2)
    y(i) = branin(x(:,i));
  end

  y_min($+1) = min(y);
  printf(' fmin = %f\n', y_min($));
  nmopt = tell(nmopt, x, y);
end 

[f_opt, x_opt] = best(nmopt);

scf;
drawlater;
xgrid(2);

X = nmopt.lower(1):(nmopt.upper(1)-nmopt.lower(1))/10:nmopt.upper(1);
Y = nmopt.lower(2):(nmopt.upper(2)-nmopt.lower(2))/10:nmopt.upper(2);
Z = [];

for i=1:length(X)
  for j=1:length(Y)
    Z(i,j) = branin([X(i),Y(j)]);
  end
end

xset('fpf',' ');
contour(X,Y,Z,10);

xtitle('Evolution of the simplex','x1','x2');

for i=4:length(nmopt.x_hist)
  plot(nmopt.x_hist(i)(1)(1), nmopt.x_hist(i)(1)(2), 'ko');
  plot(nmopt.x_hist(i)(2)(1), nmopt.x_hist(i)(2)(2), 'ko');
  plot(nmopt.x_hist(i)(3)(1), nmopt.x_hist(i)(3)(2), 'ko');
  plot([nmopt.x_hist(i)(1)(1) nmopt.x_hist(i)(2)(1)], [nmopt.x_hist(i)(1)(2) nmopt.x_hist(i)(2)(2)], 'k-');
  plot([nmopt.x_hist(i)(2)(1) nmopt.x_hist(i)(3)(1)], [nmopt.x_hist(i)(2)(2) nmopt.x_hist(i)(3)(2)], 'k-');
  plot([nmopt.x_hist(i)(3)(1) nmopt.x_hist(i)(1)(1)], [nmopt.x_hist(i)(3)(2) nmopt.x_hist(i)(1)(2)], 'k-');
end
drawnow;

printf('Done\n');
