// 1. Scilab commands can be executed in the template_replace fields.
// This is particularly useful for formating outputs.

tpl_file_name = 'template.txt';
out_file_name = 'result.txt';
var_names     = 'x';
var_values    = 0.001;

template_replace(tpl_file_name, var_names, var_values, out_file_name);
// (this generates the file result.txt)

// 2. The comma notation for numbers can be handled

out_file_name = 'result_with_comma.txt';

template_replace(tpl_file_name, var_names, var_values, out_file_name, %T);
// (this generates the file result_with_comma.txt)

