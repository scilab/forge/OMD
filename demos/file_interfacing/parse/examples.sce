// Optional behaviors of the parse function when the simulator fails
// and illustrations of type checking.


// 1. In the following exemple, the field "y" remains empty (the
// simulator could not be executed)

outfile_name = 'empty_field.txt';
outkeys      = ['x = ','y = '];
outkeys_type = ['real', 'real'];
outkeys_nbs  = [4, 1];

// hereafter, an error is issued:
// [keyval,err] = parse(outfile_name, outkeys, outkeys_type, outkeys_nbs); 
// how to handle simulator that do not fill fields
outkeys_nbs = [4, -1]; // note the negative sign

[keyval, err] = parse(outfile_name, outkeys, outkeys_type, outkeys_nbs);

of = keyval(outkeys(2), 100000) // values that have not been
                                // calculated are replaced with a
                                // default value

// 2. Another common scenario : the simulator does not produce any
// output

outfile_name = 'un_fichier_quinestpas.txt';

[keyval, err] = parse(outfile_name, outkeys, outkeys_type, outkeys_nbs);

if (err ~= 0) then
  disp('do something special when the simulator did not produce any output');
  of = 200000
end

// 3. Type checking when reading the output

outfile_name = 'type_checking.txt';
outkeys      = ['xint = ', 'yint = ', ...
	       'x = ','y = ','z = ', ...
	       'name : ','surname ','id : ','weight : '];
outkeys_type = ['integer', 'integer', ...
	       'real', 'real', 'real', ...
	       'string', 'string', 'integer', 'real'];
outkeys_nbs  = [1, 1, ...
	       1, 1, 1, ...
	       1, 1, 2, 1];

try      
  [keyval, err] = parse(outfile_name, outkeys, outkeys_type, outkeys_nbs);
catch
  printf('An error occured\n');
  [str,n,line,func]=lasterror();
  printf('Error: %s, errnum = %d linenum = %d, func = %s\n', str, n, line, func);
end


// 4. Handling of the comma notation for numbers

out_file_name = 'comma.txt';
outkeys       = ['comma : '];
outkeys_type  = ['real'];
outkeys_nbs   = [1];

[keyval,err] = parse(out_file_name , outkeys , outkeys_type , outkeys_nbs)
