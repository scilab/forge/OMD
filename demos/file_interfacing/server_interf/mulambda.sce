// (mu-lambda)-ES optimizer

// (the architecture must be loaded before executing this script)
// Adaptation du script au couplage serveur de calcul/Scilab
//cd /Users/boucard/Applications/Scientifique/CODE_SCILAB/
//exec("architecture/loader.sce");

global server_path;

server_path = get_absolute_file_path('mulambda.sce');

exec("multi.sce");

// function to optimize 
function y = f(x)
  param = struct() ;
  simu  = multi(param);
  crit  = simu(x')
  y     = crit.objective(1)
  printf('Parameters: %f and %f\n', x(1), x(2))
  printf('Cost function: %f\n",y)
endfunction

// range
xmin = [-2., 2.];
xmax = [-2., 2.];

// *** mulambda optimizer ***

// constructor
function this = mulambda(param)
  if param.mu >= param.lambda then
    error('mu must be lower than lambda')
  end

  this = mlist(['mulambda', ...
                'mu', 'lambda', 'sigma', 'x0', 'nb_max_iter', ...
                'd', 'xbar', 'iter', 'X', 'y'])
  this.mu          = param.mu;
  this.lambda      = param.lambda;
  this.sigma       = param.sigma;
  this.nb_max_iter = param.nb_max_iter;
  this.x0          = param.x0;

  this.d    = length(this.x0);
  this.xbar = this.x0;
  this.iter = 0;
  this.X    = [];
  this.y    = [];
endfunction

// 'plot2d' method
function %mulambda_plot2d(this)
  plot2d(this.X(:,1), this.X(:,2), style=-1);
  e = gce();
  e.children.mark_foreground = this.iter;
  e.children.thickness = 2;
endfunction

// 'stop' method
function out = %mulambda_stop(this)
  out = (this.iter >= this.nb_max_iter) | (this.y(1)== 0.);
endfunction

// 'best' method
function [yopt, xopt] = %mulambda_best(this)
  [yopt, i] = min(this.y);
  xopt = this.X(i,:);
  printf('Parameters: %f and %f\n', xopt(1), xopt(2))
  printf('Optimal cost function: %f\n",yopt)
endfunction

// 'ask' method
function X = %mulambda_ask(this)
  X = zeros(this.lambda, this.d);
  for i = 1 : this.lambda
    X(i,:) = this.xbar + grand(1, this.d, 'nor', 0, this.sigma);
  end
endfunction

// 'tell' method
function this = %mulambda_tell(this, X, y)
  this.X    = X;
  this.y    = y;
  this.iter = this.iter + 1;
  [y_sort, i_sort] = gsort(this.y, 'c', 'i');
  X_sel     = this.X(i_sort(1:this.mu),:);
  this.xbar = mean(X_sel, 'r');
endfunction

// *** main program ***

// initialization of the optimizer
first_iter       = %T;
opt_param        = struct();
opt_param.mu     = 1;
opt_param.lambda = 5;
opt_param.sigma  = 0.05;
opt_param.x0     = [0.15, 0.15];
opt_param.nb_max_iter = 100;
my_optimizer          = mulambda(opt_param);

// optimization loop
while ~stop(my_optimizer)
  X = ask(my_optimizer);
  
  n = size(X,1);
  y = zeros(1, n);
  for i = 1 : n
    y(i) = f(X(i,:));
  end
  
  my_optimizer = tell(my_optimizer, X, y);
  printf('Iteration: %f\n",my_optimizer.iter);	
  printf('***************\n');
end

// print optimum
best(my_optimizer)

// plot
plot2d(my_optimizer)

// quit server
unix('touch end.txt');
