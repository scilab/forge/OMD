// Attention programme tres complexe :-)
// Calcul de la fonction de Rosenbrock
// Le point important ici : input sur le stdin 
// Principe d'un programme interactif qui attend 
// des actions de l'utilisateur au clavier (comme CAST3M par exemple)
// 
// Pour l'exemple on sort la valeur de la fonction dans le fichier output.txt
//

#include <stdio.h>
#include <string.h>

int main()
{
  float x,y,roz;
  char answer;
  FILE *foutput;
  while(1)
    {
      printf("Enter GO to start or END to quit\n");
      scanf(" %s",&answer);
      if(strcmp(&answer,"END")==0)
	{
	  break;
	}
      printf("Enter X and Y values separated by a comma\n");
      scanf(" %f, %f",&x,&y);
      roz = (1.-x*x)*(1.-x*x)+100.*(y-x*x)*(y-x*x) ;
      // Histoire de faire croire que ca prend du temps...
      sleep(1);
      // Sortie dans un fichier et a l'ecran pour control	
      foutput = fopen("output.txt","w");
      fprintf(foutput,"%f \n",roz);
      // Pour eviter les problemes de cache	
      fflush(foutput);
      close(foutput);
      printf("%f \n", roz);
    }	

  return 0;
}
