// Pour l interfacage Scilab <-> serveur de calcul
//
// 
//
// PAB 10/2009
//
// Chargement des fonctions specifiques OMD
// Requirements: the OMD toolbox must be launched

//cd /Users/boucard/Applications/Scientifique/CODE_SCILAB/;
//exec("architecture/loader.sce");

server_path = get_absolute_file_path('test-server.sce');

//
// TEST DE FONCTIONNEMENT
//
// Pour faire un  calcul
// On remplace les valeurs dans le  fichier
if (%T) then
  // On lance le serveur dans une fenetre X11
  unix('xterm -sl 600 -sb -e ''cd ' + server_path + '; ./launch_server'' &')

  // Premieres valeurs
  keys   = ['X', 'Y'];
  values = [1.0, 2.0 ];
  template_replace('file_ros.tpl', keys, values, 'file.ros');
  // On lance le calcul
  unix('touch start.txt');
  // On attend les resultats
  while %T
    [x,test_file1] = fileinfo('output.txt');
    [x,test_file2] = fileinfo('end_ros.txt');
    if ( (test_file1==0) & (test_file2==0) ) then
      break
    end
  end
  // Recuperation des resultats depuis le fichier et menage
  file_lat = mopen('output.txt');
  [res1]   = mfscanf(file_lat, '%g');
  mclose(file_lat);
  
  printf('value received from server: %f\n', res1);
  
  unix('rm output.txt');
  unix('rm end_ros.txt');
  
  // On peut quitter le serveur
  unix('touch end.txt');
end
